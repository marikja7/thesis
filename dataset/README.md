# Dataset

The dataset contains two tournaments -- Ricany-2020 and Ricany-2019  with 430 photos of score sheets and corresponding PGN files.

For better evaluation purposes, score sheets were manually divided into four categories:
- **A** -- Score sheets of good quality which do not contain any additional information. Characters are not overlapping but can be slightly touching.
- **B** -- Score sheets of medium quality. They can contain overlapping chars, some scratches or additional information written by a player.
- **C** -- Score sheets that are hardly readable by humans.
- **L** -- Score sheets that are not in Czech algebraic notation.

There is no guarantee that the PGN file is complete and does not contain any incorrectly transcripted move.




