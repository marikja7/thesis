[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko U labutě"]
[Date "2019.08.26"]
[Round "4.14"]
[White "Groh, Jiri"]
[Black "Steiner, Juraj"]
[Result "0-1"]
[WhiteElo "2001"]
[BlackElo "1920"]
[ECO "A31"]
[PlyCount "0"]
[EventDate "2019.08.24"]
[Opening "English: symmetrical variation"]

1. Nf3 c5 2. d4 cxd4 3. Nxd4 Nf6 4. c4 b6 5. Nc3 Bb7 6. Bg5 e6 7. f3 a6 8. e4
d6 9. Qb3 Be7 10. O-O-O O-O 11. e5 Ne8 12. Bxe7 Qxe7 13. exd6 Qxd6 14. Kb1 Qc7
15. Bd3 Nd7 16. Qc2 g6 17. Ne4 Ne5 18. h4 Rd8 19. Nb3 Bxe4 20. Bxe4 Rxd1+
21. Rxd1 Qxc4 22. Qxc4 Nxc4 23. Bb7 a5 24. Rd4 Ncd6 25. Bc6 Nf5 26. Ra4 Ned6
27. Nd2 Rc8 28. Be4 Nxh4 29. Rd4 Nxe4 30. Nxe4 Nxg2 31. Rd7 Ne1 32. Ng5 Rf8
33. f4 h6 34. Nh3 Rc8 35. Rd6 b5 36. Ra6 Nd3 37. a3 b4 38. Rd6 Rc1+ 39. Ka2 Rd1
40. Rb6 Nc1+ 41. Kb1 b3 42. Nf2 Rf1 43. Nd1 Rxd1 0-1