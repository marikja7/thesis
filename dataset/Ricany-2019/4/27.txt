[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko U labutě"]
[Date "2019.08.26"]
[Round "4.27"]
[White "Jirousek, Tadeas"]
[Black "Otrubova, Kristyna"]
[Result "1-0"]
[WhiteElo "1856"]
[BlackElo "1712"]
[ECO "B70"]
[PlyCount "0"]
[EventDate "2019.08.24"]
[Opening "Sicilian: dragon variation"]

1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. Nc3 g6 6. Be2 Bg7 7. O-O O-O
8. Be3 Nc6 9. Kh1 d5 10. Nxc6 bxc6 11. e5 Nd7 12. f4 f6 13. exf6 Nxf6 14. Na4
Ne4 15. c3 Qc7 16. Bf3 Nd6 17. Bxd5+ Kh8 18. Bb3 Nf5 19. Qe2 e5 20. Bc5 Rd8
21. fxe5 Bxe5 22. g4 Nh6 23. Rae1 Bg7 24. h3 Bb7 25. Qg2 Rd7 26. Qf3 Ba6 27. Rf2
Rad8 28. Be6 Rd3 29. Qg2 Rd1 30. Qe4 R8d3 31. g5 Nf5 32. Bxf5 Qb8 33. Bg4 Bb5
34. Re2 Rxh3+ 35. Bxh3 Bxe2 36. Qxe2 1-0