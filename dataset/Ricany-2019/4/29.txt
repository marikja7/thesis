[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko U labutě"]
[Date "2019.08.26"]
[Round "4.29"]
[White "Dome, Michal"]
[Black "Kuhnmund, Ivan"]
[Result "1/2-1/2"]
[WhiteElo "1799"]
[BlackElo "1631"]
[ECO "B28"]
[PlyCount "0"]
[EventDate "2019.08.24"]
[Opening "Sicilian: O'Kelly variation"]

1. e4 c5 2. Nf3 a6 3. Nc3 b5 4. d4 cxd4 5. Nxd4 e5 6. Nf3 Nc6 7. Bd3 Bb4 8. O-O
Bxc3 9. bxc3 Nf6 10. Re1 O-O 11. Ba3 Re8 12. c4 Qa5 13. Bc1 b4 14. Nd2 d6
15. Bb2 Bg4 16. Be2 Be6 17. h3 Qc5 18. Nb3 Qb6 19. Bd3 a5 20. a4 bxa3 21. Bxa3
a4 22. Nd2 Na5 23. Rb1 Qc7 24. Qe2 Rec8 1/2-1/2