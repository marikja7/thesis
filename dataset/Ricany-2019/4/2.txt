[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko U labutě"]
[Date "2019.08.26"]
[Round "4.2"]
[White "Mladek, Richard"]
[Black "Rut, Vojtech"]
[Result "1-0"]
[WhiteElo "2266"]
[BlackElo "2115"]
[ECO "B03"]
[PlyCount "0"]
[EventDate "2019.08.24"]
[Opening "Alekhine's defence: exchange variation"]

1. e4 Nf6 2. e5 Nd5 3. d4 d6 4. c4 Nb6 5. exd6 exd6 6. Nc3 Nc6 7. d5 Ne5 8. Be2
Be7 9. f4 Ng6 10. Nf3 O-O 11. O-O Bg4 12. h3 Bxf3 13. Rxf3 Re8 14. g3 Bf6
15. Be3 Qd7 16. Kg2 c5 17. Bd3 Bxc3 18. bxc3 Qa4 19. Qb3 Re7 20. Bf2 Rae8 21. g4
Nf8 22. Rb1 Qa6 23. Ra1 Nfd7 24. a4 Qa5 25. g5 g6 26. h4 f6 27. Qc2 Rb8 28. Be3
Qa6 29. a5 fxg5 30. hxg5 Nxc4 31. Ra4 Nxe3+ 32. Rxe3 Rxe3 33. Bxa6 bxa6 34. Re4
Re8 35. Rxe8+ Rxe8 36. Qa4 Rd8 37. Qc6 Nf8 38. Qxa6 Rd7 39. Kf3 Kg7 40. Qc8 Kf7
41. Ke4 Ke7 42. Qb8 Kf7 43. Kd3 Ke7 44. Kc4 Kf7 45. Kb5 Ke7 46. Kc6 Rd8
47. Qxa7+ Nd7 48. Qc7 Ke8 49. c4 Nf8 50. Qxd8+ Kxd8 51. a6 1-0
