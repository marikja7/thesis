[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko U labutě"]
[Date "2019.08.26"]
[Round "4.22"]
[White "Skala, Jaromir"]
[Black "Nemecek, Lukas"]
[Result "1/2-1/2"]
[WhiteElo "1899"]
[BlackElo "1778"]
[ECO "A80"]
[PlyCount "0"]
[EventDate "2019.08.24"]
[Opening "Dutch"]

1. d4 f5 2. Nf3 d5 3. g3 Nf6 4. Bg2 e6 5. O-O Bd6 6. b3 Qe7 7. c4 c6 8. a4 O-O
9. Ba3 Nbd7 10. Bxd6 Qxd6 11. Qc2 Ne4 12. Nbd2 b6 13. Rac1 c5 14. cxd5 exd5
15. e3 Ba6 16. Rfe1 Rac8 17. Qb2 Rfe8 18. Bh3 Nxd2 19. Qxd2 g6 20. Bg2 c4 21. b4
Bb7 22. h4 Qc6 23. b5 Qd6 24. Qc3 a6 25. Ra1 a5 1/2-1/2