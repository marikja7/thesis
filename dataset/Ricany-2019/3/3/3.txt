[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko"]
[Date "2019.08.25"]
[Round "3.3"]
[White "Steiner, Juraj"]
[Black "Mladek, Richard"]
[Result "0-1"]
[ECO "E61/15"]
[WhiteElo "1920"]
[BlackElo "2266"]
[PlyCount "54"]
[EventDate "2019.08.24"]

1. d4 Nf6 2. c4 g6 3. Nc3 Bg7 4. Nf3 O-O 5. Bg5 d6 6. e3 c5 7. Be2 h6 8. Bh4 g5
9. Bg3 Nh5 10. d5 f5 11. Nd2 Nxg3 12. hxg3 e6 13. dxe6 Bxe6 14. Bf3 Nc6 15. Nd5
Rb8 16. g4 Ne5 17. gxf5 Bxf5 18. Be4 Nd3+ 19. Kf1 Nxf2 20. Kxf2 Bxe4+ 21. Kg1
Bxd5 22. cxd5 Qf6 23. Rh3 Qf2+ 24. Kh1 Rbe8 25. Nc4 Re4 26. Qd3 Rh4 27. e4 g4
0-1