[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko"]
[Date "2019.08.25"]
[Round "3.6"]
[White "Holub, Milan"]
[Black "Novotny, Martin"]
[Result "1/2-1/2"]
[ECO "E97"]
[WhiteElo "1988"]
[BlackElo "2178"]
[PlyCount "67"]
[EventDate "2019.08.24"]

1. d4 Nf6 2. Nf3 g6 3. c4 Bg7 4. Nc3 O-O 5. e4 d6 6. Be2 e5 7. O-O Nc6 8. d5
Ne7 9. Bd2 Ne8 10. Rc1 f5 11. Ng5 h6 12. Ne6 Bxe6 13. dxe6 c5 14. exf5 Nxf5 15.
Bf3 Nc7 16. Bxb7 Rb8 17. Bd5 Qe7 18. Qg4 Kh7 19. b3 Nxe6 20. Ne4 Nfd4 21. Rc3
Nf4 22. Bxf4 Rxf4 23. Qd1 a5 24. g3 Rf5 25. Rd3 Bf8 26. Nc3 Bg7 27. Be4 Rbf8
28. Nd5 Qf7 29. f4 Kg8 30. Ne3 Qe7 31. Nxf5 gxf5 32. Bd5+ Kh8 33. fxe5 dxe5 34.
g4 1/2-1/2