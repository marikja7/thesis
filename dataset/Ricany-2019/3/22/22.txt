[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko"]
[Date "2019.08.25"]
[Round "3.22"]
[White "Voracek, Zdenek"]
[Black "Dome, Michal"]
[Result "1-0"]
[ECO "D22"]
[WhiteElo "1938"]
[BlackElo "1799"]
[PlyCount "89"]
[EventDate "2019.08.24"]

1. d4 d5 2. c4 dxc4 3. Nf3 a6 4. e3 b5 5. a4 Bb7 6. b3 cxb3 7. axb5 axb5 8.
Rxa8 Bxa8 9. Bxb5+ c6 10. Bc4 e6 11. Qxb3 Nf6 12. O-O Be7 13. Ba3 O-O 14. Ne5
Bxa3 15. Qxa3 Nbd7 16. Nd3 Qc7 17. Nd2 c5 18. dxc5 Ng4 19. g3 Qc6 20. e4 Nge5
21. Nxe5 Nxe5 22. Be2 Rc8 23. Rc1 Nd7 24. f3 Bb7 25. Nb3 Qc7 26. Bb5 Ra8 27.
Qb4 Ne5 28. c6 Bc8 29. f4 Ng6 30. e5 Ne7 31. Kh1 h5 32. Qd6 Ra7 33. Qxc7 Rxc7
34. Nd4 Nf5 35. Nxf5 exf5 36. Kg2 Kf8 37. Kf3 Ke7 38. Ke3 Kd8 39. Rd1+ Ke7 40.
Kd4 Ba6 41. Bxa6 Rxc6 42. Bc4 h4 43. Rb1 hxg3 44. hxg3 g5 45. Rb7+ 1-0