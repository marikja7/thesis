[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko"]
[Date "2019.08.25"]
[Round "3.23"]
[White "Toulec, Stanislav"]
[Black "Paulovic, Milan"]
[Result "1-0"]
[ECO "A00"]
[WhiteElo "1930"]
[BlackElo "1788"]
[PlyCount "41"]
[EventDate "2019.08.24"]

1. e4 c5 2. Nc3 Nc6 3. Nf3 d6 4. d4 cxd4 5. Nxd4 Nf6 6. Be2 g6 7. Be3 Bg7 8.
Qd2 a6 9. f3 O-O 10. h4 Qc7 11. g4 Be6 12. h5 Nxd4 13. Bxd4 Bc4 14. hxg6 fxg6
15. g5 e5 16. gxf6 exd4 17. fxg7 Rf7 18. Qxd4 Bxe2 19. Kxe2 Rxg7 20. Rad1 Rd8
21. Qd5+ 1-0