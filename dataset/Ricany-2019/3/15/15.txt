[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko"]
[Date "2019.08.25"]
[Round "3.15"]
[White "Zvolensky, David"]
[Black "Reljic, Michael"]
[Result "1-0"]
[ECO "A00"]
[WhiteElo "2152"]
[BlackElo "1890"]
[PlyCount "99"]
[EventDate "2019.08.24"]

1. d4 d5 2. c4 e6 3. Nf3 Nf6 4. g3 c5 5. Bg2 Nc6 6. cxd5 exd5 7. O-O cxd4 8.
Nxd4 Be7 9. Nc3 O-O 10. h3 h6 11. Be3 Be6 12. Qa4 Qd7 13. Nxe6 fxe6 14. Rfd1
Ne5 15. Qxd7 Nfxd7 16. Rac1 Kh8 17. b3 a6 18. Na4 Rac8 19. Nb6 Rc6 20. Nxd7
Nxd7 21. Rxc6 bxc6 22. h4 Kg8 23. Bc1 Bc5 24. e3 Kf7 25. Bb2 g6 26. Rc1 Rc8 27.
e4 Rf8 28. b4 Bxb4 29. Rxc6 dxe4 30. Rc7 Ke7 31. Bg7 Rf7 32. Bxh6 Bc5 33. Be3
Bxe3 34. fxe3 Kd6 35. Ra7 Kd5 36. Bf1 a5 37. Bb5 Kc5 38. Rxd7 Rf3 39. a4 Rxg3+
40. Kf2 Rf3+ 41. Ke2 Rh3 42. Rg7 Rxh4 43. Rxg6 Rh2+ 44. Ke1 Rh3 45. Kd2 e5 46.
Rc6+ Kb4 47. Rc4+ Kb3 48. Rxe4 Rh5 49. Kd3 Rg5 50. Rh4 1-0