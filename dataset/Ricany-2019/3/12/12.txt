[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko"]
[Date "2019.08.25"]
[Round "3.12"]
[White "Vojta, Jakub"]
[Black "Elias, Jakub"]
[Result "1-0"]
[ECO "B41/01"]
[WhiteElo "2256"]
[BlackElo "1836"]
[PlyCount "53"]
[EventDate "2019.08.24"]

1. e4 c5 2. Nf3 d6 3. d4 cxd4 4. Nxd4 Nf6 5. f3 a6 6. c4 e6 7. Nc3 Be7 8. Be3
Qc7 9. Be2 O-O 10. O-O Nbd7 11. Qd2 b6 12. Rac1 Bb7 13. Rfd1 Rac8 14. Kh1 Qb8
15. Bf1 Rfd8 16. Qf2 Ne5 17. Na4 Ba8 18. Nb3 Nfd7 19. Nxb6 Nxb6 20. Bxb6 Rd7
21. a4 Bd8 22. a5 Bxb6 23. axb6 Rc6 24. c5 dxc5 25. Nxc5 Rxd1 26. Rxd1 Qxb6 27.
Nxa6 1-0