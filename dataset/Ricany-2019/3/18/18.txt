[Event "Pohár města Říčany 2018"]
[Site "Městské kulturní středisko"]
[Date "2019.08.25"]
[Round "3.18"]
[White "Maly, Filip"]
[Black "Horyna, Josef"]
[Result "1-0"]
[ECO "C11"]
[WhiteElo "1941"]
[BlackElo "1763"]
[PlyCount "85"]
[EventDate "2019.08.24"]

1. e4 e6 2. d4 d5 3. Nc3 Nf6 4. e5 Nfd7 5. f4 c5 6. Nf3 Nc6 7. Be3 cxd4 8. Nxd4
Nxd4 9. Bxd4 a6 10. Qd2 Be7 11. Bd3 Nc5 12. O-O-O Nxd3+ 13. Qxd3 Bd7 14. Kb1
Bc6 15. Rhf1 Qd7 16. Qe3 O-O-O 17. Bb6 Rdf8 18. Ne4 Kb8 19. c4 Qc8 20. Nd6 Bxd6
21. exd6 Qd7 22. Bc7+ Ka8 23. c5 f6 24. Qc3 Rf7 25. Qa5 Rhf8 26. Rf3 g5 27. Rb3
Qc8 28. Rd4 gxf4 29. Rxf4 e5 30. Rf1 d4 31. Rb6 Be4+ 32. Ka1 Bxg2 33. c6 Bxc6
34. Rc1 Be4 35. b4 d3 36. b5 d2 37. Qxd2 Rg8 38. Qa5 Rxc7 39. dxc7 Bd3 40. bxa6
Bxa6 41. Rxa6+ bxa6 42. Qd5+ Ka7 43. Qxg8 1-0