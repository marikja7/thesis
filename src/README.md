# How to run the application

The application is implemented as a frontend-backend application. To run the application, you must do following steps:

1. Install python 3.7 -- https://www.python.org/downloads/
2. Install Node.js -- https://nodejs.org/en/
3. Install PostgreSql -- https://www.postgresql.org/download/
4. Create PostgreSql databese.
5. Set .env file in score-sheet-backend:
    *  SECRET_KEY='your secret key'
    *  SQLALCHEMY_DATABASE_URI='[DB_TYPE]+[DB_CONNECTOR]://[USERNAME]:[PASSWORD]@[HOST]:[PORT]/[DB_NAME]'
6. Run Flask
    * Open command line in folder './score-sheet-backend'
    * Install all requirements from requirements.txt:
        ```
        pip install -r /path/to/requirements.txt
        ```
    * run flask: 
        ```
        flask run
        ```
7. Run React 
    * Open command line in folder './score-sheet-frontend'
    * Run react by command: 
        ```
        npm  start
        ```
        
# How to run scripts

1. Install python 3.7 -- https://www.python.org/downloads/
2. Install jupyter notebook
    ```    
    pip install notebook
    ```
3. Open command line in folder './src/notebooks'
4. Write command into the opened command line:
     ```    
    jupyter notebook
    ```