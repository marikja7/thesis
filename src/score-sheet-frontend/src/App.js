import React from 'react';

import './App.css'
import CustomBar from "./component/CustomBar";
import {BrowserRouter, Route} from "react-router-dom";
import Home from './component/Home/Home'
import 'bootstrap/dist/css/bootstrap.min.css';
import Board from "./component/SheetConvert/Board/Board";
import TemplateUpload from "./component/Template/TemplateUpload/TemplateUpload";
import SheetConvert from "./component/SheetConvert/SheetConvert";


export default function App() {

  return (
      <div className="app">
        <CustomBar/>
        <BrowserRouter>
                <Route exact path="/" component={Home} />
                <Route path="/convert" component={Board} />
                <Route path="/loadImage" component={TemplateUpload} />
                <Route path="/scImage" component={SheetConvert} />
        </BrowserRouter>
      </div>
  );
}

