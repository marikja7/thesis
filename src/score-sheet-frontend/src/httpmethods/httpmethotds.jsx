import axios from "axios"

export const saveTemplate = (template) => {
    axios.post(`/store_template`, template).then(console.log(''))
}

export const convertScoreSheet = (templateId, scoreSheet, setMoves, setSSLoaded) => {
    const request = axios.post( `/convertScoreSheet`, {template_id: templateId, img: scoreSheet})
    request.then(({data}) =>  {setMoves({'id': data.id, 'moves': data.moves}); setSSLoaded(1)} )
}

export const getTemplates = (setTemplates, setImage) => {
    const request = axios.get(`/templates`)
    request.then(({data}) => {
        setTemplates(data.images)
        switch (data.images.length) {
            case 0: setImage({act: -1}); break;
            case 1: setImage({act: 0}); break;
            case 2: setImage({act: 0, prev: 1, next: 1}); break;
        }
    })
}

export const repairGame = (game_id, move, setMoves) => {
    const request = axios.post(`/repair_game`, {game_id: game_id, move: move})
    request.then(({data}) =>  {setMoves({'id': data.id, 'moves': data.moves}) })
}

export const evaluatePosition = (position, setPositionInfo) => {
    const request = axios.post(`/evaluate_position`, {position: position})
    request.then(({data}) =>  {setPositionInfo({
        score: data['score'],
        bestMoves: data['mv']
    }) })
}


export const hello = () => {
    console.log('jsem tu')
    const request = axios.get(`/hello`)
    request.then(({data}) =>  { console.log(data) })
}