import React from 'react';
import { Nav, Navbar} from 'react-bootstrap'

import './CustomBar.css'

export default function CustomBar() {
    return (
        <Navbar className="navbarCustom" bg="dark" variant="dark">
            <Navbar.Brand href="/">ChessSheet</Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/loadImage">Template</Nav.Link>
                <Nav.Link href="/scImage">Convert</Nav.Link>
            </Nav>
        </Navbar>

    );
}

