import React from "react";
import './UploadFile.css'

export default function UploadFile({fileUpload}) {


    const handleChange = ({target}) => {
        let img = document.createElement('img');
        img.target = target.files[0];
        img.src =  URL.createObjectURL(target.files[0]);
        fileUpload(img);
    }

    return (
        <div className="col-md-12 text-center">
            <div className="upload-btn-wrapper">
                <button className="my_btn">
                        <i className="fas fa-upload"></i>
                        &nbsp;
                        Upload file
                </button>
                <input type="file" name="myfile"
                       onChange={handleChange}/>
            </div>
        </div>
    )
}