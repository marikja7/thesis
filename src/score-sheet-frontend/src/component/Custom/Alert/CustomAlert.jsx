import React from "react";
import {Alert} from "react-bootstrap";
import './CustomAlert.css'

export default function CustomAlert({alert}) {
    const renderAlert = () =>
        alert.show ?
            <Alert
                variant={alert.variant}>
                {alert.text}
                <div className="cross"><i className="fa fa-times"></i></div>
            </Alert>
            :
            ''

    return(
        <div>
            {renderAlert()}
        </div>
    )
}