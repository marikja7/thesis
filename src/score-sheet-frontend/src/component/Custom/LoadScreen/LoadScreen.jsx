import React from "react";

export default function LoadScreen() {
    return(
        <div>
            <i className="fas fa-cog fa-spin"></i> Computing ...
        </div>
    )
}