import {Col, Row} from "react-bootstrap";
import React from "react";
import './Footer.css'

export default function Footer() {
    return (
        <Row className="footer">
            <Col md={4}>
                <h6><i className="far fa-address-book"></i> &nbsp;&nbsp; Contact</h6>
                <h6><i className="fas fa-user-shield"></i> &nbsp; Privacy</h6>
                <h6><i className="far fa-clipboard"></i> &nbsp;&nbsp; Terms and Conditions </h6>
            </Col>
            <Col md={4}>
                <h6><i className="fas fa-envelope"></i>&nbsp; asdasd@asdasd.com</h6>
                <h6><i className="fab fa-facebook-messenger"></i> &nbsp; messenger</h6>
                <h6><i className="fas fa-mobile-alt"></i>&nbsp; +420 666 666 666</h6>
            </Col>
            <Col md={4}>
                <h6><i className="far fa-file-code"></i>&nbsp; GitHub</h6>
                <h6><i className="fas fa-book"></i>&nbsp; Documentation</h6>
                <h6><i className="fab fa-youtube"></i>&nbsp; Video instruction</h6>
            </Col>
        </Row>

    )


}