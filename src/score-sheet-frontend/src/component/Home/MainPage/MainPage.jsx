import background from "../../../img/pexels-george-becker-112854.jpg";
import {Button, Row} from "react-bootstrap";
import React from "react";
import {useHistory} from "react-router-dom";
import './MainPage.css'


export default function MainPage({statsRef}) {
    const history = useHistory();

    const scollPage = () => {
        window.scrollTo({
            top: statsRef.current.offsetTop,
            behavior: 'smooth'
        })
    }

    return (
        <Row className="main_page">
            <img  className="background_photo" src={background} alt={background}/>

            <div className="main_page_content">
                <h1>Chess Score Sheet Converter </h1>
                <h5>Let your computer convert your chess score sheet in Four simple steps!</h5>
                <br/>
                <div className="buttons">
                    <Button variant="secondary" className="bottom_frontpage" size="lg"
                            onClick={scollPage}>
                        How it works
                    </Button>
                    &nbsp;&nbsp;&nbsp;
                    <button className="my_btn" onClick={() => history.push('/scImage')} > Lets Start</button>
                </div>
            </div>
        </Row>
    )
}