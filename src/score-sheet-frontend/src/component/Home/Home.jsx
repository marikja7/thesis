import React, {useState} from 'react';
import {Col} from "react-bootstrap";
import Instructions from "./Instructions/Instructions";
import MainPage from "./MainPage/MainPage";
import Footer from "../Footer/Footer";
import "./Home.css"
export default function Home() {

    const [statsRef] = useState(React.createRef())

    return  (
        <Col>
            <MainPage statsRef={statsRef}/>
            <Instructions statsRef={statsRef}/>
            <Footer/>
        </Col>
    )


}