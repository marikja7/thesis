import React, {useEffect, useState} from "react";
import {Container } from 'react-bootstrap'
import "../../Template/TemplateUpload/TemplateUpload.css"
import './ScoreSheetLoad.css'
import UploadFile from "../../Custom/UploadFile/UploadFile";
import {convertScoreSheet, getTemplates} from '../../../httpmethods/httpmethotds'
import {getBase64} from "../../../utils/base64";
import TemplateSelect from "../TemplateSelect/TemplateSelect";
import LoadScreen from "../../Custom/LoadScreen/LoadScreen";


export default function ScoreSheetLoad({setSSLoaded, setMoves}) {
    const [templates, setTemplates] = useState(null)
    const [act_img, setActImg] = useState({prev: 0, act: 1, next: 2})
    const [computing, setComputing] = useState(0)

    useEffect(_ => {
        getTemplates(setTemplates, setActImg)

    }, [getTemplates])


    const fileUpload = (img) => {
        setComputing(1)
        getBase64(img?.target, (result) => {
            convertScoreSheet(templates[act_img.act].id, result, setMoves, setSSLoaded)
        });
    }

    const renderLoading = () => {
        if(computing) return <LoadScreen/>
    }


    if(!templates) {
        return <div>Loading...</div>
    }

    return (
        <div>
            <TemplateSelect templates={templates} act_img={act_img} setActImg={setActImg}/>
            <h3>2. Upload score sheet</h3>
            <div className="upload_scoresheet">
                <UploadFile fileUpload={fileUpload}/>
            </div>
            {renderLoading()}
        </div>
    )
}

