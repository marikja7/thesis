import React from "react";
import {Button, Col, Row} from 'react-bootstrap'
import "./TemplateSelect.css"

const IMAGE_PREFIX = 'data:image/jpeg;base64,'

export default function TemplateSelect({templates, act_img, setActImg}) {


    const renderTemplates = () => {
        console.log(templates?.length)
        switch(templates?.length) {
            case 0: return <h5>No templates has been uploaded yet.</h5>
            case 1: return <img className="selected_img" id={templates[0]?.id} width={150} src={`${IMAGE_PREFIX}${templates[0]?.img}`} alt="" />
            case 2: return <div>
                                <img id={templates[act_img.prev]?.id} width={100} src={`${IMAGE_PREFIX}${templates[act_img.prev]?.img}`} alt="" />
                                &nbsp;
                                &nbsp;
                                <img className="selected_img" id={templates[act_img.act]?.id} width={150} src={`${IMAGE_PREFIX}${templates[act_img.act]?.img}`} alt="" />
                                &nbsp;
                                &nbsp;
                                <img id={templates[act_img.next]?.id} width={100} src={`${IMAGE_PREFIX}${templates[act_img.prev]?.img}`} alt=""/>
                            </div>
            default:
                return <div>
                            <img id={templates[act_img.prev]?.id} width={100} src={`${IMAGE_PREFIX}${templates[act_img.prev]?.img}`} alt="" />
                            &nbsp;
                            &nbsp;
                            <img className="selected_img" id={templates[act_img.act]?.id} width={150} src={`${IMAGE_PREFIX}${templates[act_img.act]?.img}`} alt="" />
                            &nbsp;
                            &nbsp;
                            <img id={templates[act_img.next]?.id} width={100} src={`${IMAGE_PREFIX}${templates[act_img.next]?.img}`} alt=""/>
                        </div>
        }
    }

    const  onNextClick = () => {
        const len = templates?.length
        if(len <= 1) return
        if(len === 2)
            setActImg({prev: act_img.act,
                act: act_img.prev,
                next: act_img.act})
        else
            setActImg({prev: (act_img.prev + 1) % len,
                act: (act_img.act + 1) % len,
                next: (act_img.next + 1) % len})

    }

    const  onPrevClick = () => {
        const len = templates?.length
        if(len <= 1) return
        if(len === 2) {
            setActImg({prev: act_img.act,
                act: act_img.prev,
                next: act_img.act})
        }
        else
            setActImg({prev: (act_img.prev - 1) % len,
                act: (act_img.act - 1) % len,
                next: (act_img.next - 1) % len})
    }

    return (
        <div>

            <h3>1. Choose template</h3>
            <div className="cont">

                <Row lassName="justify-content-md-center">
                    <Col xs={3} className="all_btn">
                        <Button variant="secondary" onClick={onPrevClick} className="btn_secondary">
                            <i className="fas fa-chevron-left"></i>
                        </Button>
                    </Col>

                    <Col xs={6} className="col_img">
                        <Row className="templates">
                            {renderTemplates()}
                        </Row>
                        <Row className="num">
                            {(act_img.act+1) + '/' + templates?.length}
                        </Row>
                    </Col>
                    <Col xs={3} className="all_btn">
                        <Button variant="secondary" onClick={onNextClick} className="btn_secondary">
                            <i className="fas fa-chevron-right"></i>
                        </Button>
                    </Col>

                </Row>

            </div>
        </div>

    )
}

