import React, {useState} from 'react';
import {Card, Table} from "react-bootstrap";
import './MovesTable.css'
import EditHeader from "./EditHeader/EditHeader";

export default function MovesTable({moves, actMove, playerInfo, setPlayerInfo}) {
    const [edit, setEdit] = useState(false)

    const isSelected = (cnt) =>
        actMove === cnt ? "selected" : "non_selected"

    const scrollPage = () => {
        var element_id = actMove
        if(actMove % 2 === 0)
            element_id -= 1
        if(actMove === 0)
            element_id = 1
        const element = document.getElementById(element_id)
        if(element) {
            element.scrollIntoView({behavior: "smooth", block: "nearest", inline: "nearest"});
        }
    }


    const rndMvs = () => {
        if(moves.length % 2)
            moves[moves.length] = ""

        const rnd_mvs = []
        for(let i = 0; i < moves.length; i += 2) {
            rnd_mvs[i] =
                    <tr id={i+1}>
                        <td>{i/2+1}.</td>
                        <td className={isSelected(i+1)}>{moves[i]}</td>
                        <td  className={isSelected(i+2)}>{moves[i+1]}</td>
                    </tr>
       }
       return <Table size="sm" className="move_table">
           <tbody>
           {rnd_mvs}
           </tbody>
       </Table>
    }

    const header = () => {
        if(edit) {
            return <EditHeader
                        playerInfo={playerInfo}
                        setPlayerInfo={setPlayerInfo}
                        setEdit={setEdit}
                    />
        }
        else {
            return (
                    <div className="header_content">
                        <div>
                            <h5>{playerInfo.white} - {playerInfo.black}</h5>
                        </div>
                        <div>
                            <h5>{playerInfo.result}</h5>
                        </div>
                        <div className="edit" onClick={() => setEdit(true)}>
                            <i className="far fa-edit"></i>
                        </div>
                    </div>
            )
        }
    }

    return(
            <Card className="cardMoves">
                {scrollPage()}
                <Card.Header className="header">
                    {header()}
                </Card.Header>
                <Card.Body className="bodycard">
                    {rndMvs()}
                </Card.Body>
            </Card>
    )
}