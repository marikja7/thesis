import React from "react";
import  {Form} from "react-bootstrap";
import { useForm } from "react-hook-form";

import './EditHeader.css'

export default function EditHeader({playerInfo, setPlayerInfo, setEdit}) {

    const { register, handleSubmit } = useForm();

    const onSubmit = data => {
        setPlayerInfo({
            white: data.white,
            black: data.black,
            result: data.result
        })
        setEdit(false)
    }

    return (
        <Form>
            <div className="editHeader">
                <Form.Control name="white"
                              ref={register}
                              placeholder={playerInfo.white}
                              defaultValue={playerInfo.white}
                              className="name"/>
                -
                <Form.Control name="black"
                              ref={register}
                              placeholder={playerInfo.black}
                              defaultValue={playerInfo.black}
                              className="name"/>

                <Form.Control name="result"
                              ref={register}
                              className="result" as="select"
                              defaultValue={playerInfo.result}>
                    <option>1-0</option>
                    <option>0-1</option>
                    <option>1/2-1/2</option>
                </Form.Control>
                <div className="edit" onClick={handleSubmit(onSubmit)}>
                    <i className="fas fa-check"></i>
                </div>
            </div>
        </Form>
    )
}