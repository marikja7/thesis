import {Button, ButtonGroup} from "react-bootstrap";
import React, {useState} from "react";
import './BoardOptions.css';
import Chess from "chess.js";
import {evaluatePosition} from "../../../httpmethods/httpmethotds";


export default function BoardOptions({moves, chess, setChess, actMove,  setActMove, playerInfo}) {

    const [positionInfo, setPositionInfo] = useState({})

    // scroll
    const nextMove = () => {
        if(actMove === moves.length)
            return
        chess.move(moves[actMove])
        setActMove(actMove+1)
        setChess(chess)
    }

    // scroll
    const prevMove = () => {
        if(actMove === 0)
            return
        chess.undo()
        setActMove(actMove-1)
        setChess(chess)
    }
    const setStart = () => {
        setChess(new Chess())
        setActMove(0)
    }
    const setEnd = () => {
        for(let i = actMove; i < moves.length; i++) {
            chess.move(moves[i])
        }
        setChess(chess)
        setActMove(moves.length)
    }

    const pgnFormat = () => {
        let pgn =
            '[Event ""]' + '\n' +
            '[Site ""]' + '\n' +
            '[Date ""]' + '\n' +
            '[Round ""]' + '\n' +
            '[White "' + String(playerInfo.white) + '"]\n' +
            '[Black "' + String(playerInfo.black) + '"]\n' +
            '[Result "' + String(playerInfo.result) + '"]\n' +
            '[WhiteElo ""]' + '\n' +
            '[BlackElo ""]' + '\n' +
            '[ECO ""]' + '\n' +
            '[PlyCount ""]' + '\n' +
            '[EventDate ""]' + '\n' +
            '[Opening ""]' + '\n';
        const new_chess = new Chess()
        for(let i = 0; i < moves.length; i++) {
            new_chess.move(moves[i])
        }
        pgn += String(new_chess.pgn())
        return pgn + ' ' +  String(playerInfo.result)
    }


    const downloadTxtFile = () => {
        const element = document.createElement("a");
        const file = new Blob([ pgnFormat()], {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = "myFile.txt";
        document.body.appendChild(element); // Required for this to work in FireFox
        element.click();
    }

    const showCoputerMoves = () => {
        evaluatePosition(chess.fen(), setPositionInfo)
        return "pracuje se na tom"
    }

    return(
        <div className="options_row">
            <div>
            <ButtonGroup>
                <Button variant="secondary" className ="btn_secondary" onClick={setStart}>
                    <i className="fas fa-step-backward" ></i>
                </Button>
                <Button variant="secondary" className ="btn_secondary" onClick={prevMove}>
                    <i className="fas fa-chevron-left" ></i>
                </Button>

                <Button variant="secondary" className ="btn_secondary" onClick={nextMove}>
                    <i className="fas fa-chevron-right" ></i>
                </Button>
                <Button variant="secondary" className ="btn_secondary" onClick={setEnd}>
                    <i className="fas fa-step-forward"></i>
                </Button>
            </ButtonGroup>
            </div>

            <div className="dw_options">
            <ButtonGroup >
                <Button variant="secondary" className ="btn_secondary"  onClick={showCoputerMoves}>
                    <i className="fas fa-laptop"></i>
                </Button>
                <Button variant="secondary" className ="btn_secondary"  onClick={downloadTxtFile}>
                    <i className="fas fa-download"></i>
                </Button>
            </ButtonGroup>
            </div>
        </div>

    )
}