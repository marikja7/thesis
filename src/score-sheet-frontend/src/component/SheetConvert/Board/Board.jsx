import React, {useState} from 'react';
import {Container} from 'react-bootstrap'
import Chess from 'chess.js'
import Chessboard from "chessboardjsx";
import './Board.css'
import MovesTable from "../MovesTable/MovesTable";
import '../../Custom/Custom.css'
import rough from 'roughjs'
import BoardOptions from "../BoardOptions/BoardOptions";

export default function Board({moves, changeMove, actMove, setActMove,  playerInfo, setPlayerInfo}) {
    const [chess, setChess] = useState(new Chess());


    const roughSquare = ({ squareElement, squareWidth }) => {
        let rc = rough.svg(squareElement);

        const chessSquare = rc.rectangle(0, 0, squareWidth, squareWidth, {
            roughness: 0.5 ,
            fill: "#f9f3fc",
            bowing: 2,
            fillStyle: "hacure"
        });
        squareElement.appendChild(chessSquare);
    }

    const makeMove = ({sourceSquare, targetSquare}) => {

        const move = chess.move({ from: sourceSquare, to: targetSquare})
        if(move == null) {
            return
        }
        changeMove(actMove, move.san)
        setActMove(actMove+1)
    }

    return (
        <Container >
                <div className="board_row">
                    <div>
                    <Chessboard
                        id="standard"
                        width={450}
                        roughSquare={roughSquare}
                        position={chess.fen()}
                        onDrop={ (move) => makeMove(move)}
                        transitionDuration={150}
                        draggable={true}
                        undo={true}
                        boardStyle={{
                            borderRadius: "5px",
                            boxShadow: `0 5px 15px rgba(0, 0, 0, 0.5)`
                        }}
                        sparePieces={false}
                        lightSquareStyle={{ backgroundColor: "#f9f3fc" }}
                        darkSquareStyle={{ backgroundColor: "#8730ac" }}
                    />

                    <br/>

                    <div className="board_options">
                        <BoardOptions
                            moves={moves}
                            chess={chess}
                            setChess={setChess}
                            actMove={actMove}
                            setActMove={setActMove}
                            playerInfo={playerInfo}
                        />
                    </div>

                    </div>
                    <MovesTable className="move_table"
                        moves={moves}
                        actMove={actMove}
                        playerInfo={playerInfo}
                        setPlayerInfo={setPlayerInfo}
                    />
                </div>
        </Container>
    );
}

