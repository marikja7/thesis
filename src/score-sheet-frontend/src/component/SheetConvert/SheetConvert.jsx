import React, {useState} from "react";
import {Col, Container} from 'react-bootstrap'
import "../Template/TemplateUpload/TemplateUpload.css"
import './ScoreSheetLoad/ScoreSheetLoad.css'
import ScoreSheetLoad from "./ScoreSheetLoad/ScoreSheetLoad";

import Board from "./Board/Board";
import {repairGame} from "../../httpmethods/httpmethotds";
import Footer from "../Footer/Footer";
import './SheetConvert.css'


export default function SheetConvert() {
    const [ssLoaded, setSSLoaded] = useState(0)
    const [playerInfo, setPlayerInfo] = useState({white: 'Unknown', black: 'Unknown', result: '1-0'})
    const [moves, setMoves]  = useState({id: 0, moves: ['d4', 'Nf6', 'c4', 'g6', 'Nc3', 'Bg7', 'e4', 'd6', 'Nf3', 'O-O', 'Be2', 'e5', 'O-O', 'Nc6', 'd5', 'Ne7',
            'Ne1', 'Nd7', 'Be3', 'f5', 'f3', 'f4', 'Bf2', 'g5', 'c5', 'Nxc5', 'b4', 'Na6']})
    const [actMove, setActMove] = useState(0)


    const changeMove = (cnt, san_move ) => {
        repairGame(moves.id, {cnt: cnt, move: san_move}, setMoves)
        setActMove(cnt)
    }

    const rndContent = () =>
        ssLoaded ?
            <Board moves={moves.moves}
                   changeMove={changeMove}
                   actMove={actMove}
                   setActMove={setActMove}
                   playerInfo={playerInfo}
                   setPlayerInfo={setPlayerInfo}

            />
            :
            <ScoreSheetLoad setSSLoaded={setSSLoaded} setMoves={setMoves}/>

    return (
        <Col>
            <Container>
                <div className="template_page">
                    {rndContent()}
                </div>
            </Container>
            <Footer/>
        </Col>
    )
}

