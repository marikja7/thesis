import React, {useState} from "react";
import {Col, Container} from 'react-bootstrap'
import CustomStepper from "../CustomStepper/CustomStepper";
import UploadFile from "../../Custom/UploadFile/UploadFile";
import CustomAlert from "../../Custom/Alert/CustomAlert";
import ImageBox from "../ImageBox/ImageBox";
import {saveTemplate} from '../../../httpmethods/httpmethotds'
import {convert_base64, resizeImg} from "../../../utils/rotateImage";
import FinishPage from "../Finish/FinishPage";
import Footer from "../../Footer/Footer";

const IMAGE_UPLOAD  = 0
const FINISH = 3



export default function TemplateUpload() {
    const [img, setImg] = useState({});
    const [frame, setFrame] = useState({})

    const [step, setStep] = useState(0);
    const [buttonName, setButtonName] = useState('invisible')


    const [alert, setAlert] = useState({
        variant: 'danger',
        text: 'Choose more points!',
        show: 0
    })

    const nextStep = () => {
        if(step === 2) {
            storeTemplate()
        }
        setStep(step+1)
    }
    const prevStep = () => setStep(step-1)

    const fileUpload = (uploadImage) => {
        // setISmallerImg(resizeImg(uploadImage))
        setImg(uploadImage)
        setButtonName('btn_secondary')
        nextStep()
    }

    const renderUpload = () => {
        // return <FinishPage/>
        if(step === IMAGE_UPLOAD) {
            return <div className="upload_template"><UploadFile fileUpload={fileUpload}/></div>
        }
        if(step === FINISH) {
            return <FinishPage/>
        }
        return <ImageBox step={step}
                         img={img}
                         setImg={setImg}
                         setFrame={setFrame}
                         setAlert={setAlert}
                         nextStep={nextStep}
                         prevStep={prevStep}
                         buttonName={buttonName}/>
    }


    const storeTemplate = () => {
        saveTemplate({img: convert_base64(img), frame: frame})
    }

    return (
        <Col>
            <Container>
                <div className="template_page">
                <h3 className="header_text">Load Template</h3>
                    {/*<br/>*/}
                {/*<span> If you dont know what to do see detailed instruction below.</span>*/}
                <div className="page_content">
                    <CustomStepper step={step}/>
                    <div className="load_template_content">
                    {renderUpload()}
                    </div>
                    <CustomAlert alert={alert}/>
                </div>
                </div>

            </Container>
            {/*<TemplateInstructions/>*/}
            <Footer/>
        </Col>
    )
}

