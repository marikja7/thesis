import {Col, Row} from "react-bootstrap";
import num1 from "../../../img/1.png";
import photo from "../../../img/smartphone.png";
import template_img from "../../../img/website.png";
import num2 from "../../../img/2.png";
import num3 from "../../../img/3.png";
import upload from "../../../img/submit.png";
import replay from "../../../img/checkmate.png";
import num4 from "../../../img/4.png";
import React from "react";


export default function TemplateInstructions({statsRef}) {
    return (
        <div>
            <Row  ref={statsRef} className="instruction_row">
                <img className="number" src={num1} alt={num1}/>
                <Col>
                    <h4>Scan template of your score sheet</h4>
                    <h6> Scanned Score Sheet gives better results. Recommended resolution is 1000x1123</h6>
                </Col>
                <Col>
                    <img  className="img_photo" src={photo} alt={photo}/>
                </Col>
            </Row>

            <Row className="instruction_row number_right">
                <Col>
                    <img className="img_left" src={template_img} alt={template_img}/>
                </Col>
                <Col>
                    <h4>Select proper template</h4>
                    <h6>When there is no template that matches the score sheet you must go to section Template and load scene of your template. </h6>
                </Col>
                <img className="number" src={num2} alt={num2}/>
            </Row>

            <Row className="instruction_row">
                <img className="number" src={num3} alt={num3}/>
                <Col>
                    <h4>Upload Score Sheet</h4>
                    <h6>Select Score Sheet from Your computer or other device and upload it. </h6>
                </Col>
                <Col>
                    <img className="img_upload" src={upload} alt={upload}/>
                </Col>

            </Row>
            <Row className="instruction_row number_right">
                <Col>
                    <img className="img_board" src={replay} alt={replay}/>
                </Col>
                <Col>
                    <h4> Replay game on the board or download it</h4>
                    <h6> You can replay your game on the board and also correct misclassified moves.</h6>
                </Col>
                <img className="number" src={num4} alt={num4}/>
            </Row>
        </div>

    )
}