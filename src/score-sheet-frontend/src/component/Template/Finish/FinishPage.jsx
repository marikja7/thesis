import React from "react";
import {Alert} from "react-bootstrap";
import './FinishPage.css'

export default function TemplateUpload() {
    return (
        <Alert className="template_finish_alert"variant="success">
            <Alert.Heading>Action was successful!</Alert.Heading>
            <p>
                Your template was successfuly upload and store. If you wnat to continue and load another template go here.
                If you want to start converting your score sheets go here.
            </p>
        </Alert>
    );
}