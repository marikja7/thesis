import {Button, Col, Row} from "react-bootstrap";
import React  from "react";
import ImageRotate from "../ImageRotate/ImageRotate";
import AreaSelector from "../AreaSelector/AreaSelector";
import './ImageBox.css'


const IMAGE_ROTATE = 1
const AREA_SELECTOR = 2


export default function ImageBox({step, img, setImg, setFrame, setAlert,
                                     nextStep, prevStep, buttonName}) {

    const getFrame = (frame) => {
        setFrame(frame)
    }

    const onDoneClick = () => {
        setAlert({
            variant: '',
            text: '',
            show: 0
        })
        nextStep(step+1)
    }

    const onPrevClick = () => prevStep(step)

    const renderImage = () => {
        // eslint-disable-next-line default-case
        if(step === IMAGE_ROTATE)
            return <ImageRotate img={img} setImg={setImg}/>
        if(step === AREA_SELECTOR)
            return <AreaSelector step={step} img={img} getFrame={(frame) => setFrame(frame)}/>

    }


    const getNext = () => {
        if(step === AREA_SELECTOR) {
            return (
                <Col xs={3} className="all_btn">
                    <Button variant="secondary" onClick={onDoneClick} className={buttonName}>
                        Finish <i className="fas fa-chevron-right"></i>
                    </Button>
                </Col>
            )
        }
        return (
            <Col xs={3} className="all_btn">
                <Button variant="secondary" onClick={onDoneClick} className={buttonName}>
                    <i className="fas fa-chevron-right"></i>
                </Button>
            </Col>
        )


    }

    return (
        <div className="cont">
            <Row lassName="justify-content-md-center">
                <Col xs={3} className="all_btn">
                    <Button variant="secondary" onClick={onPrevClick} className={buttonName}>
                        <i className="fas fa-chevron-left"></i>
                    </Button>
                </Col>

                <Col xs={6} align="center">
                    {renderImage()}
                </Col>
                {getNext()}
            </Row>
        </div>
    )
}