import React, {useState} from "react";
import {Image, Button} from "react-bootstrap";
import {rotateImage} from "../../../utils/rotateImage";
import './ImageRotate.css'
import '../ImageBox/ImageBox.css'

export default function ImageRotate({img, setImg}) {
    const [rotation, setRotation] = useState(0)
    const rotate = () => {
        setImg(rotateImage(img))
        setRotation(rotation+1)
    }

    return (
           <div>
                <Image
                    id ="img"
                    src={img?.src}
                    width={300}
                    className='image_table'
                />
                <Button variant="secondary" className="rotate_btn" onClick={rotate}>
                    <i className="fas fa-redo"></i>
                </Button>
           </div>
    )
}