import React from "react";
import Stepper from'react-stepper-horizontal'
import './CustomStepper.css'

const STEP1 = {title: 'Load Image'}
const STEP2 = {title: 'Rotate image'}
const STEP3 = {title: 'Select moves area'}
const STEP4 = {title: 'Done!'}
const STEPS = [STEP1, STEP2, STEP3, STEP4]

export default function CustomStepper({step}) {

    return (
        <div className="stepper">
            <Stepper steps={STEPS} activeStep={step}
                     circleFontSize={20}
                     size={40}
                     activeColor="#7b1fa2"
                     completeColor="#ce93d8"
                     />
        </div>
    )
}