import React, {useState} from "react";
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import './AreaSelector.css'
export default function AreaSelector({img, getFrame})  {
    const [crop, setCrop] = useState({});

    const handleChange = (crop) => {
        setCrop(crop)
        //class="ReactCrop__image"
        // Name of image in ReactCrop
        var img_blem = document.getElementsByClassName("ReactCrop__image");
        let coeff_y = img.width/img_blem[0].clientWidth
        let coeff_x = img.height/img_blem[0].clientHeight
        getFrame({
            y: Math.ceil(crop.y*coeff_y),
            x: Math.ceil(crop.x*coeff_x),
            height: Math.ceil(crop.height*coeff_y),
            width: Math.ceil(crop.width*coeff_y)
        })
    }

    return (
            // <div i>
                <ReactCrop className="crop" src={img?.src} crop={crop} onChange={handleChange}/>
            // </div>
    )
}