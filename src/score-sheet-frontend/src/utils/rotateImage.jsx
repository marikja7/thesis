


const dataURItoBlob = (dataURI) => {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

// Rotate image by 90 degree
export const  rotateImage = (img) =>  {
    const canvas = document.createElement("canvas");
    let maxDim = Math.min(img.height, img.width);
    if(img.height > img.width)
        maxDim = Math.max(img.height, img.width);
    canvas.width = img.height;
    canvas.height = img.width;
    const ctx = canvas.getContext("2d");
    ctx.setTransform(1, 0, 0, 1, maxDim/2, -maxDim /2);
    ctx.rotate(90 * (Math.PI / 180));
    ctx.drawImage(img, maxDim/2, -maxDim /2);
    const blob = dataURItoBlob(canvas.toDataURL('image/jpeg'));
    img.src = URL.createObjectURL(blob);
    img.innerHTML = blob.size + ' bytes';
    return img
}

export const  resizeImg = (img) =>  {
    var canvas = document.createElement('canvas'), ctx = canvas.getContext('2d');

    // set its dimension to target size
    canvas.width = 100;
    canvas.height = 150;

    // draw source image into the off-screen canvas:
    ctx.drawImage(img, 0, 0, 100, 150)
    const blob = dataURItoBlob(canvas.toDataURL('image/jpeg'));
    img.src = URL.createObjectURL(blob);
    img.innerHTML = blob.size + ' bytes';
    return img
}


export const convert_base64 = (sourceimage) => {
    // sourceimage  = sourceimage.src
    console.log(sourceimage)
    var canvas = document.createElement("canvas");
    canvas.height = canvas.width = 0;
    var context = canvas.getContext('2d');
    var imgwidth = sourceimage.width;
    var imgheight = sourceimage.height;
    console.log(imgheight)
    console.log(imgwidth)
    canvas.width = imgwidth;
    canvas.height = imgheight;
    context.drawImage(sourceimage, 0, 0);
    console.log('base64: ', canvas.toDataURL('image/jpeg'))
    return canvas.toDataURL('image/jpeg')
}