from math import ceil
from logic.Img_Move import Img_Move
from utils.Frame import Frame
from utils.Coordinates import Coordinates
from utils.Custom_img import crop_image


def sorted_boxes(template, warped):
    # Template shape - score sheet shape could be in different shape -> coordninates must be normalized
    sc_height, sc_width = warped.shape
    temp_height = (template.y_bottom - template.y_up) + 80
    temp_width = template.x_right - template.x_left
    diff = Coordinates(sc_height / temp_height, sc_width / temp_width)
    return sorted(template.move_boxes, key=lambda x: x.cnt, reverse=False), diff


def get_frame(template, box, diff):
    return Frame(ceil(diff.y * (box.y - template.y_up)),
                 ceil(diff.y * (box.y - template.y_up + template.move_height)),
                 ceil(diff.x * (box.x - template.x_left)),
                 ceil(diff.x * (box.x - template.x_left + template.move_width)))


class Img_Moves:
    def __init__(self, temp, im):
        self.moves = []
        moves = []
        boxes, diff = sorted_boxes(temp, im)
        half_move_height = int(diff.x * temp.move_height / 2)
        width, sum_bl, height = [0]*3
        for i, box in enumerate(boxes):
            f = get_frame(temp, box, diff)
            crop_img = im[f.y_up:(f.y_down + half_move_height), f.x_left:f.x_right]
            only_move_are, _ = crop_image(im[f.y_up:f.y_down, f.x_left:f.x_right])

            mv = Img_Move(crop_img, i)

            if not mv.valid(sum_bl, width, height):
                break
            width += only_move_are.shape[1]
            height += only_move_are.shape[0]
            sum_bl += len(only_move_are[only_move_are == 255])
            moves.append(mv)
        if len(moves) == 0:
            return
        width /= len(moves)
        width /= 3.2
        for i, mv in enumerate(moves):
            mv.extract_chars(width)
            self.moves.append(mv)

