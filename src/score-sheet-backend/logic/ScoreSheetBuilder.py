from utils.Convert_img import base64_to_matrix
import numpy as np
import cv2

MIN_MATCH_COUNT = 10

def template_match(template_img, image):
    sift = cv2.SIFT_create()

    kp1, des1 = sift.detectAndCompute(template_img, None)
    kp2, des2 = sift.detectAndCompute(image, None)

    flann = cv2.FlannBasedMatcher({'algorithm': 1, 'trees': 5}, {'checks': 50})
    matches = flann.knnMatch(des1, des2, k=2)
    # store all the good matches as per Lowe's ratio test.
    good = [
        m for m, n in matches if m.distance < 0.7 * n.distance
    ]
    if len(good) < MIN_MATCH_COUNT:
        print("Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT))
        return
    src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
    H, _ = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
    return H


# https://github.com/methylDragon/opencv-python-reference/blob/master/02%20OpenCV%20Feature%20Detection%20and%20Description.md

def build_score_sheet(img, template):
    template_img = base64_to_matrix(template.img)
    template_img = cv2.cvtColor(template_img, cv2.COLOR_BGR2GRAY)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # obtain a consistent order of the points and unpack them
    # individually
    crop_img = template_img[template.y_up:template.y_bottom + 80, template.x_left:template.x_right]
    H = template_match(img, crop_img)
    return cv2.warpPerspective(img, H, (crop_img.shape[1], crop_img.shape[0]))
