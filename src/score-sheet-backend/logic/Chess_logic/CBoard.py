import chess

from logic.Chess_logic.CMove import CMove

SCALE = 10000


def costumize_move(move):
    return move.replace('+', '').replace('#', '').replace('=', '')


class CBoard:
    def __init__(self):
        self.board = chess.Board()
        self.legal_moves = self.get_legal_moves()

    def get_legal_moves(self):
        moves = str(self.board.legal_moves).split('(')[1].replace(')', '').replace(' ', '').replace('>', '').replace(
            '+', '').replace('#', '').split(',')
        if moves == ['']:
            return None
        return moves

    def make_move(self, move):
        if costumize_move(move) not in self.legal_moves:
            return 0
        self.board.push_san(move)
        self.legal_moves = self.get_legal_moves()
        return 1

    def make_random_move(self):
        if self.legal_moves is None:
            return None
        str_move = self.legal_moves[0]
        self.make_move(str_move)
        return str_move

    def candidates_moves(self, moves):
        if self.legal_moves is None:
            return None
        return [
            CMove(m.str_move, m.prob)
            for m in moves
            if m.str_move in self.legal_moves
        ]
