from logic.Chess_logic.CBoard import CBoard
from logic.Chess_logic.CMoves import CMoves


class CGame:
    def __init__(self, predicted_chars=None):
        self.board = CBoard()
        if predicted_chars is None:
            return
        self.cmoves = CMoves(predicted_chars)
        self.game = []
        self.create_game(0)

    def create_game(self, index):
        print(len(self.cmoves.moves))
        for index_moves in range(index, len(self.cmoves.moves)):
            pos_moves = self.cmoves.moves[index_moves]
            pos_moves = self.board.candidates_moves(pos_moves)
            if not pos_moves:
                move = self.board.make_random_move()
                if move is None:
                    return
            else:
                move = pos_moves[0].str_move
                self.board.make_move(str(move))
            self.game.append(move)

    def restore_game(self, moves):
        moves = sorted(moves, key=lambda move: move.cnt, reverse=False)
        self.cmoves = CMoves()
        self.cmoves.restore_moves(moves)
        self.game = [m.move for m in moves if m.act_line == 1]

    def repair_game(self, index, str_move):
        self.game = self.game[:index]
        for move in self.game:
            self.board.make_move(move)
        self.board.make_move(str_move)
        self.game.append(str_move)
        self.create_game(index + 1)
