from constants.notation_dictionaries import DEFAULT_PROB
from constants.pseudo_legal_moves import PSEUDO_LEGAL_MOVES

from logic.Chess_logic.CMove import CMove


def containsAny(str, set):
    """ Check whether sequence str contains ANY of the items in set. """
    if not set:
        return 1
    return 1 in [c in str for c in set]


import numpy as np


def substitution_value(org_char, new_char, prob_chars_values, index):
    if index >= len(prob_chars_values):
        return 1
    prob_chars_values = prob_chars_values[index]
    if org_char == new_char:
        return 0
    prob1 = DEFAULT_PROB[org_char]
    prob2 = DEFAULT_PROB[new_char]
    for index, prob in enumerate(prob_chars_values):
        if str(prob[0]) == str(org_char):
            prob1 += prob[1]
        if str(prob[0]) == str(new_char):
            prob2 += prob[1]
    return abs(prob1 - prob2)


def delete_value(org_char, prob_chars_values, index):
    prob1 = DEFAULT_PROB[org_char]
    prob_chars_values = prob_chars_values[index]
    for index, prob in enumerate(prob_chars_values):
        if str(prob[0]) == str(org_char):
            return prob1 + prob[1]
    return prob1


def levenshtein(seq1, seq2, prob_chars_values):
    size_x = len(seq1) + 1
    size_y = len(seq2) + 1
    matrix = np.zeros((size_x, size_y))
    for x in range(size_x):
        matrix[x, 0] = x
    for y in range(size_y):
        matrix[0, y] = y

    for x in range(1, size_x):
        for y in range(1, size_y):
            if seq1[x - 1] == seq2[y - 1]:
                matrix[x, y] = min(
                    matrix[x - 1, y] + delete_value(seq1[x - 1], prob_chars_values, x - 1),  # insert
                    matrix[x - 1, y - 1],
                    matrix[x, y - 1] + 1
                )
            else:
                matrix[x, y] = min(
                    matrix[x - 1, y] + delete_value(seq1[x - 1], prob_chars_values, x - 1),  # insert
                    matrix[x - 1, y - 1] + substitution_value(seq1[x - 1], seq2[y - 1], prob_chars_values, x - 1),
                    # substitution
                    matrix[x, y - 1] + 1
                    # delete_value(seq1[x-1], prob_chars_values, x - 1)  # delete
                )
    return (matrix[size_x - 1, size_y - 1])


class CMoves:
    # get all possible combination of predicted chars in one move
    # compare them with pseudo legal moves and get similarity

    def __init__(self, predicted_chars=None):
        self.moves = []
        if predicted_chars is None:
            return

        for move in predicted_chars:
            hight_prob_chars = []

            prob = 1
            mv = ''
            for char_index in range(len(move)):
                mv += str(move[char_index][0][0])
                hight_prob_chars.append(move[char_index][0][0])
                hight_prob_chars.append(move[char_index][1][0])
                prob *= move[char_index][0][1]

            comb = []
            for str_mv in PSEUDO_LEGAL_MOVES:
                if not containsAny(str_mv, hight_prob_chars):
                    continue
                str_mv = str_mv.replace('-', '')
                if mv != '' and (mv[-1] == '+' or mv[-1] == '#'):
                    dist = levenshtein(mv[:-1], str_mv, move)
                else:
                    dist = levenshtein(mv, str_mv, move)
                comb.append(CMove(str_mv, prob - dist))
            comb = sorted(comb, reverse=True)[:10]
            self.moves.append(comb)

    def restore_moves(self, moves):
        moves = sorted(moves, key=lambda move: move.cnt)
        max_cnt = moves[-1].cnt

        for i in range(max_cnt):
            mvs = [CMove(m.move, m.prob) for m in moves if m.cnt == i]
            self.moves.append(sorted(mvs, key=lambda move: move.prob, reverse=True))
