
class CMove:
    def __init__(self, str_move, prob):
        self.str_move = str_move
        self.prob = prob

    def __lt__(self, other):
        return self.prob < other.prob
