from utils.Coordinates import Coordinates
from logic.segmentation.Vertical_projection import Vertical_projection
import cv2

# --------------------------------------------------------------------------------------------#

# Hyper parameters:

MIN_WIDTH = 10
MIN_HEIGHT = 15
MAX_WIDTH = 40
MAX_HEIGHT = 40

# Thinning?
MIN_AVG_DENSITY = 7


# --------------------------------------------------------------------------------------------#

class CComponent:
    def __init__(self, label, coordinates, padding, max):
        self.label = label
        self.coordinates = coordinates.copy()
        self.padding = padding
        self.max = max
        if self.padding.x == -1:
            self.padding, self.max = self.find_extremes()

    def width(self):
        return self.max.x - self.padding.x + 1

    def height(self):
        return self.max.y - self.padding.y + 1

    def normalize_coord(self, coord):
        return Coordinates(coord.y - self.padding.y, coord.x - self.padding.x)

    def upper_range(self, move_height):
        max_coord, min_coord = -1, -1
        for coord in self.coordinates:
            # Change -40
            if coord.y < move_height * (2 / 3):
                if coord.x > max_coord:
                    max_coord = coord.x
                if coord.x < min_coord or min_coord == -1:
                    min_coord = coord.x
        return min_coord, max_coord

    # Predelat
    # def remove_coords_in_range(self, ):
    def remove_coords_in_range(self, min_coord, max_coord, self_height):
        new_coords = [
            coord for coord in self.coordinates
            if min_coord < coord.x < max_coord
        ]
        self.coordinates = new_coords.copy()
        self.padding, self.max = self.find_extremes()

    def vertical_projection(self):
        return Vertical_projection(self)

    def remove_coordinates(self, rem_coordinates):
        self.coordinates = [
            c for c in self.coordinates if c not in rem_coordinates
        ]
        self.padding, self.max = self.find_extremes()

    def split_by_column(self, column):
        coords1, coords2 = [], []
        # print('column: ', column)
        for c in self.coordinates:
            if c.x < column:
                coords1.append(c)
            if c.x > column:
                coords2.append(c)
        # print('coordinates: ', coords1)
        # print('coordinates: ', coords2)
        return CComponent(1, coords1, Coordinates(-1, -1), Coordinates(-1, -1)), \
               CComponent(2, coords2, Coordinates(-1, -1), Coordinates(-1, -1))

    def valid_minimum(self, min_height, min_width, min_size):
        return self.size() >= min_size \
               and self.height() >= min_height \
               and self.width() >= min_width

    def valid(self):
        return MAX_WIDTH > self.width() > MIN_WIDTH and MAX_HEIGHT > self.height() > MIN_HEIGHT

    def set_label(self, label):
        self.label = label
        return self

    def size(self):
        return len(self.coordinates)

    def min_distance(self, comp):
        min_dist = -1
        for coord in comp.coordinates:
            for c in self.coordinates:
                dist = c.distance(coord)
                if dist < min_dist or min_dist == -1:
                    min_dist = dist
        return min_dist

    def merge(self, comp):
        self.label, comp.label = min(comp.label, self.label), max(comp.label, self.label)
        self.padding.x = min(comp.padding.x, self.padding.x)
        self.padding.y = min(comp.padding.y, self.padding.y)
        self.max.x = max(comp.max.x, self.max.x)
        self.max.y = max(comp.max.y, self.max.y)
        self.coordinates.extend(comp.coordinates)

    def coverage_x(self, comp):
        # No coverage
        if comp.padding.x > self.max.x or self.padding.x > comp.max.x:
            return 0
        # Absolute coverage
        if (self.padding.x <= comp.padding.x and self.max.x >= comp.max.x) \
                or (self.padding.x >= comp.padding.x and self.max.x <= comp.max.x):
            return 100
        coverage = comp.max.x - self.padding.x
        if self.max.x < comp.max.x:
            coverage = self.max.x - comp.padding.x
        absolute = self.width() + comp.width() - coverage
        return (100 / absolute) * coverage

    def coverage_y(self, comp):
        # No coverage
        if comp.padding.y > self.max.y or self.padding.y > comp.max.y:
            return 0
        # Absolute coverage
        if (self.padding.y <= comp.padding.y and self.max.y >= comp.max.y) \
                or (self.padding.y >= comp.padding.y and self.max.y <= comp.max.y):
            return 100
        coverage = comp.max.y - self.padding.y
        if self.max.y < comp.max.y:
            coverage = self.max.y - comp.padding.y
        absolute = self.height() + comp.height() - coverage
        return (100 / absolute) * coverage

    def above(self, comp, threshold):
        # print('Testing above')
        # dist = self.min_distance(comp)
        # print('distance: ', dist)
        # if dist > 15:
        #     return False
        coverage_x = self.coverage_x(comp)
        if coverage_x < threshold:
            return False
        coverage_y = self.coverage_y(comp)
        if coverage_y > 40:
            return False
        return True

    def find_extremes(self):
        min_x, min_y, max_x, max_y = [-1] * 4
        for coord in self.coordinates:
            if coord.x < min_x or min_x == -1:
                min_x = coord.x
            if coord.x > max_x or max_x == -1:
                max_x = coord.x
            if coord.y < min_y or min_y == -1:
                min_y = coord.y
            if coord.y > max_y or max_y == -1:
                max_y = coord.y
        return Coordinates(min_y, min_x), Coordinates(max_y, max_x)

    def img(self):
        import numpy as np
        img = np.ones((self.height(), self.width()), np.uint8) * 255
        for coord in self.coordinates:
            norm_coord = self.normalize_coord(coord)
            img[norm_coord.y][norm_coord.x] = 0

        height, width = img.shape
        scale_height = int(height * (1 / 10))
        scale_width = int(width * (1 / 10))
        img = cv2.copyMakeBorder(img, scale_height, scale_height, scale_width, scale_width, cv2.BORDER_CONSTANT,
                                 value=255)
        img = cv2.resize(img, (28, 28))
        return 255 - img
