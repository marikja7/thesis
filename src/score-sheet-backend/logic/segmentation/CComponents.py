from logic.segmentation.CComponent import CComponent
from utils.Coordinates import Coordinates
import cv2

#################################################################################

# Hyper parameters:
MIN_DISTANCE = 5

MIN_WIDTH = 15
MIN_HEIGHT = 15
MIN_SIZE = 20

MAX_WIDTH = 45
MAX_HEIGHT = 50

# Thinning?
MIN_AVG_DENSITY = 7


#################################################################################


class CComponents:
    def __init__(self, output, shape):

        img_component = output[1]
        self.height, self.width = img_component.shape
        (numLabels, labels, stats, centroids) = output
        self.sum_bl = len(img_component[img_component != 0])
        self.components = []

        # self.components = [0] * (numLabels - 1)
        # Component with label 0 is background
        for comp in range(1, numLabels):
            x = stats[comp, cv2.CC_STAT_LEFT]
            y = stats[comp, cv2.CC_STAT_TOP]
            width = stats[comp, cv2.CC_STAT_WIDTH]
            height = stats[comp, cv2.CC_STAT_HEIGHT]
            area = stats[comp, cv2.CC_STAT_AREA]
            if area < 10 or width < 10 or height < 10:
                continue

            coordinates = [
                Coordinates(h, w)
                for w in range(x, x + width)
                for h in range(y, y + height)
                if img_component[h][w] == comp
            ]
            self.components.append(
                CComponent(comp - 1, coordinates, Coordinates(y, x), Coordinates(y + height, x + width))
            )
        self.height, self.width = shape

    def remove_noise(self):
        self.remove(
            [
                c.label for c in self.components
                if c.size() < MIN_SIZE or c.height() < MIN_HEIGHT or c.width() < MIN_WIDTH
            ]
        )

    def remove(self, remove):
        self.components = [cp for cp in self.components if cp.label not in remove]
        self.sort_by_x()

    def remove_below_upper_line(self):
        self.remove([
            cp.label for cp in self.components
            if (cp.padding.y > self.height * (2 / 3))
               or (cp.padding.y <= 2 and cp.max.y <= self.height * (1 / 3))
        ])

    def merge(self, merge):
        comp1 = self.components[merge[0]]
        comp2 = self.components[merge[1]]
        comp1.merge(comp2)
        self.remove([comp2.label])

        # Vertical merge from left to right

    def vertical_merge(self, percentage):
        index = 0
        while index < len(self.components) - 1:
            comp = self.components[index]
            next_comp = self.components[index + 1]
            if next_comp.above(comp, percentage) or comp.above(next_comp, percentage):
                self.merge((comp.label, next_comp.label))
            index += 1

    # Horizontal merge from left to right
    def horizontal_merge(self, MIN_DIST, par1, par2):
        index = 0
        while index < len(self.components) - 1:
            comp1 = self.components[index]
            comp2 = self.components[index + 1]
            if comp1.min_distance(comp2) < MIN_DIST \
                    and comp1.size() + comp2.size() < self.sum_bl / par1 \
                    and comp1.width() + comp2.width() < self.width / par2:
                self.merge((comp1.label, comp2.label))
            else:
                index += 1

    def force_split(self):
        if len(self.components) == 0:
            return False
        comp1 = self.components[0]
        column = int(comp1.width() / 2)
        c1, c2 = comp1.split_by_column(column)
        self.remove([comp1.label])
        self.add([c1, c2])
        return True

    def add(self, components):
        self.components.extend(components)
        self.sort_by_x()

    def vertical_split(self, avg_char_lenght):
        threshold_size = 1.5
        ran = len(self.components)
        for i in range(ran):

            c = self.components[i]
            # if c.label == 0:
            #     threshold_size = 2
            if c.width() > avg_char_lenght * threshold_size:
                vert_projection = c.vertical_projection()
                psc = vert_projection.evaluate_psc()
                for column in psc:
                    comp1, comp2 = c.split_by_column(column + c.padding.x)
                    if comp1.width() > avg_char_lenght * 0.6 and comp2.width() > avg_char_lenght * 0.6:
                        self.remove([c.label])
                        self.add([comp1, comp2])
                        break

    def horizontal_remove(self):
        for c in self.components:
            if c.max.y > self.height * 0.8:
                min_coord, max_coord = c.upper_range(self.height)
                c.remove_coords_in_range(min_coord, max_coord, self.height)

    def sort_by_x(self):
        self.components.sort(key=lambda comp: comp.padding.x)
        for i, c in enumerate(self.components):
            self.components[i] = c.set_label(i)
