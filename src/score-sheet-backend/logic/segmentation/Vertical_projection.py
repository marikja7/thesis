import numpy as np

class Vertical_projection:
    def __init__(self, component):
        self.ver_density = [0] * component.width()
        for coord in component.coordinates:
            norm_coord = component.normalize_coord(coord)
            self.ver_density[norm_coord.x] += 1

    # Find potentional segmentation column based on vertical projection
    def evaluate_psc(self):
        self.psc = [i for i, elem in enumerate(self.ver_density) if elem < 15]
        hist, bin_edges = np.histogram(self.psc, density=True)

        return [int(i) for i in bin_edges]
