import numpy as np

from tensorflow.keras.models import load_model
from constants.notation_dictionaries import translate
import keras.backend.tensorflow_backend as tb

tb._SYMBOLIC_SCOPE.value = True
ROOT_PATH = './logic/cnn_models/models/'


def preprocess(img):
    img = img.astype('float32') / 255.0
    array = np.array([img])
    array = array.reshape(array.shape[0], 28, 28, 1)
    return array.astype('float32')


class Model:
    def __init__(self):
        self.model = load_model(ROOT_PATH + 'LeNet50.h5')
        print('Loaded model')

    def predict_moves(self, moves):
        predicted_moves, predicted_move = [], []

        for index_m, m in enumerate(moves.moves):
            for c in m.chars:
                img = preprocess(c.img())
                res = self.model(img)
                index_res = np.argsort(res[0])[::-1][:10]
                prob_res = np.sort(res[0])[::-1][:10]
                predicted_move.append(
                    [
                        (translate(i), prob_res[index])
                        for index, i in enumerate(index_res)
                    ]
                )
            predicted_moves.append(predicted_move)
            predicted_move = []
        return predicted_moves
