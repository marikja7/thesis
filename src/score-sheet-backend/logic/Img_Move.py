from logic.segmentation.CComponents import CComponents
import cv2


# https://github.com/methylDragon/opencv-python-reference/blob/master/02%20OpenCV%20Feature%20Detection%20and%20Description.md

class Img_Move:
    def __init__(self, img, cnt):
        self.img = img
        self.height, self.width = self.img.shape
        self.cnt = cnt
        self.chars = []

    def valid(self, sum_bl, width, height):
        move_part_img = self.img[0:int(self.height * 2 / 3), 0:self.width]
        if self.cnt == 0:
            return 1
        return len(move_part_img[move_part_img == 255]) > (sum_bl / self.cnt) / 3

    def extract_chars(self, avg_char_lenght):
        output = cv2.connectedComponentsWithStats(self.img, 4, cv2.CV_32S)
        components = CComponents(output, self.img.shape)
        components.remove_below_upper_line()
        # components.remove_noise()
        # components.horizontal_remove()
        # components.vertical_merge(60)
        # components.vertical_split(avg_char_lenght)
        components.remove_noise()
        components.sort_by_x()
        if len(components.components) < 2:
            components.force_split()
        self.chars = [c for c in components.components]
