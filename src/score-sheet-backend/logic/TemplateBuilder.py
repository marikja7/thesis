import cv2
import numpy as np
from utils.Convert_img import matrix_to_base64
from utils.Coordinates import Coordinates
from utils.Frame import Frame
from collections import Counter
from utils.utils import percentage

THRESHOLD_PERCENT = 2
LINE_WIDTH = 3

def is_empty(img, x, y, w, h):
    for ix in range(x + 10, x + w - 10):
        for iy in range(y + 10, y + h - 10):
            if img[iy][ix] == 0:
                return False
    return True

def move_shape(heights, widths):
    h, w = Counter(heights), Counter(widths)
    return h.most_common(1)[0][0], w.most_common(1)[0][0]

def find_up_move(box, boxes, move_height):
    for b in boxes:
        if box[1] - move_height + int(move_height)/2 > b[1] and \
        box[1] - move_height - int(move_height)/2 < b[1] and \
            box[0] + int(box[2])/2 > b[0] and box[0] - int(box[2])/2 < b[0]:
            return True
    return False

def find_down_move(box, boxes, move_height):
     for b in boxes:
        if box[1] + move_height + int(move_height)/2 > b[1] and \
        box[1] + move_height - int(move_height)/2 < b[1] and \
            box[0] + int(box[2])/2 > b[0] and box[0] - int(box[2])/2 < b[0]:
            return True
     return False

def valid_shape(box, move_height, move_width):
    return not (box[3] + int(move_height/2) <= move_height or box[3] - int(move_height/2) >= move_height or \
                box[2] + int(move_width/2) <= move_width or box[2] - int(move_width/2) >= move_width)

def create_move_boxes(bounding_box, move_height, move_width,
                            y_up, y_bottom, x_left, x_right):
    change = 0
    while 1:
        len_boxes = len(bounding_box)
        for index_box in range(len_boxes-1):
            box = bounding_box[index_box]
            if not valid_shape(box, move_height, move_width):
                bounding_box.remove(box)
                change = 1
                break
            if box[1] - 2*move_height > y_up:
                if not find_up_move(box, bounding_box, move_height):
                    bounding_box.append((box[0], box[1]-2*LINE_WIDTH-move_height, move_width, move_height))
                    change  = 1
                    break
            if box[1]+move_height + 2*move_height < y_bottom+move_height:
               if not find_down_move(box, bounding_box, move_height):
                    bounding_box.append((box[0], box[1]+2*LINE_WIDTH+move_height, move_width, move_height))
                    change = 1
                    break
        if change == 1:
           change = 0
           continue
        return bounding_box


def filer_bounding_boxes(bounding_boxes, y_up, y_bottom, x_left, x_right, img):
    rect = img.copy()
    cv2.rectangle(rect, (x_left, y_up), (x_right, y_bottom), (0, 0, 255), 2)

    threshold = Coordinates(percentage(y_bottom, THRESHOLD_PERCENT), percentage(x_right, THRESHOLD_PERCENT))
    widths, heights = ([] for _ in range(2))
    filtered_boxes = []
    img_filer= img.copy()

    for bounding_box in bounding_boxes:
        x, y, w, h = bounding_box
        #TODO
        if w < 40 and h < 30:
            continue
        if x + w - threshold.x > x_right or x + threshold.x < x_left:
             continue
        if y + h - threshold.y > y_bottom or y + threshold.y < y_up:
            continue
        if w < 2 * h or not is_empty(img, x, y, w, h):
             continue
        widths.append(w), heights.append(h)
        cv2.rectangle(img_filer, (x, y), (x+w, y+h), (0, 0, 255), 2)
        filtered_boxes.append((x, y, w, h))
    cv2.imwrite('asdsad.jpg', img_filer)
    move_height, move_width = move_shape(heights, widths)
    return filtered_boxes, move_height, move_width

def create_boxes(boxes):
    x_coords, y_coords = [], []
    # LEPE!!!
    for b in boxes:
        if not (b[0] in x_coords or b[0]+1 in x_coords or b[0]-1 in x_coords \
            or b[0]+2 in x_coords or b[0]-2 in x_coords) :
            x_coords.append(b[0])
        if not (b[1] in y_coords or b[1]+1 in y_coords or b[1]-1 in y_coords \
                or b[1]+2 in y_coords or b[1]-2 in y_coords or b[1]+3 in y_coords or b[1]-3 in y_coords):
            y_coords.append(b[1])

    moves = []
    columns, rows = len(x_coords), len(y_coords)
    xs = np.sort(np.array(x_coords))
    ys = np.sort(np.array(y_coords))
    for index_x, x in enumerate(xs):
        for index_y, y in enumerate(ys):
            cnt = int(index_x) % 2
            act_column = (index_x-cnt)/2
            cnt += (act_column*rows*2) + index_y*2
            moves.append([int(cnt), int(x), int(y)])
    return moves, xs, ys

def detect_boxes(img, x, y, width, height):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    (thresh, img_bin) = cv2.threshold(img, 128, 255,
                                      cv2.THRESH_BINARY | cv2.THRESH_OTSU)  # Thresholding the image
    img_bin = 255 - img_bin  # Invert the image

    # Defining a kernel length
    kernel_length = np.array(img).shape[1] // 40

    # A verticle kernel of (1 X kernel_length), which will detect all the verticle lines from the image.
    verticle_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, kernel_length))
    # A horizontal kernel of (kernel_length X 1), which will help to detect all the horizontal line from the image.
    hori_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_length, 1))
    # A kernel of (3 X 3) ones.
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    # Morphological operation to detect verticle lines from an image
    img_temp1 = cv2.erode(img_bin, verticle_kernel, iterations=3)
    verticle_lines_img = cv2.dilate(img_temp1, verticle_kernel, iterations=3)
    # Morphological operation to detect horizontal lines from an image
    img_temp2 = cv2.erode(img_bin, hori_kernel, iterations=3)
    horizontal_lines_img = cv2.dilate(img_temp2, hori_kernel, iterations=3)
    # Weighting parameters, this will decide the quantity of an image to be added to make a new image.
    alpha = 0.5
    beta = 1.0 - alpha
    # This function helps to add two image with specific weight parameter to get a third image as summation of two image.
    img_final_bin = cv2.addWeighted(verticle_lines_img, alpha, horizontal_lines_img, beta, 0.0)
    img_final_bin = cv2.erode(~img_final_bin, kernel, iterations=2)
    (thresh, img_final_bin) = cv2.threshold(img_final_bin, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    # Find contours for image, which will detect all the boxes
    contours, hierarchy = cv2.findContours(img_final_bin, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # Sort all the contours by top to bottom. - asi nemusim
    bounding_boxes = [cv2.boundingRect(c) for c in contours]
    return filer_bounding_boxes(bounding_boxes, y, y+height, x, x+width, img)

def build_template(img, y, x, height, width):
    # displayed image has different size than send image
    filtered_boxes, move_height, move_width = detect_boxes(img, x, y, width, height)
    #y_up, y_bottom, x_left, x_right
    boxes = create_move_boxes(filtered_boxes, move_height, move_width, y, y+height, x, x+width)
    boxes, columns, rows  = create_boxes(boxes)
    return boxes, columns, rows, move_width, move_height