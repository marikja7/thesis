import cv2
import numpy as np
from skimage.filters import threshold_sauvola


def repair_lines(img):
    # Repair image after horizontal and vertical lines removal
    repair_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 6))
    return 255 - cv2.morphologyEx(255 - img, cv2.MORPH_CLOSE, repair_kernel, iterations=1)


def erase_lines(img, detected_lines):
    contours = cv2.findContours(detected_lines, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[0] if len(contours) == 2 else contours[1]
    for c in contours:
        cv2.drawContours(img, [c], -1, (255, 255, 255), 2)
    return img


def erase_kernel(img, struct):
    thresh = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, struct)
    detected_lines = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
    return erase_lines(img, detected_lines)


def remove_lines(img):
    # Remove horizontal
    img = erase_kernel(img, (50, 1))

    # Remove vertical
    img = erase_kernel(img, (1, 25))

    return repair_lines(img)


def threshold(img):
    image = np.array(img)
    thresh_sauvola = threshold_sauvola(image, window_size=25)
    img = image > thresh_sauvola
    img = img.astype(np.uint8)
    img[img == 1] = 255
    return img


def clean_image(img):
    img = threshold(img)
    img = remove_lines(img)
    return 255 - img
