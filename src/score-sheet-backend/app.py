"""Initialize Flask app."""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# from ddtrace import patch_all
from keras.engine.saving import load_model

db = SQLAlchemy()
# patch_all()

ROOT_PATH = './logic/cnn_models/models/'
def create_app():
    """Construct the core application."""
    model = load_model(ROOT_PATH + 'LeNet50.h5')
    app = Flask(__name__, instance_relative_config=False)
    # , static_folder = './static/build', static_url_path = '/'
    app.config.from_object("config.Config")

    db.init_app(app)

    with app.app_context():
        import routes  # Import routes
        db.create_all()  # Create database tables for our data models
        return app
