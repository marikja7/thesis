from flask import current_app as app
from flask import jsonify, request
from evaluate.evaluate import *
from rhandler.requests import Template_rq, Score_sheet_rq, Game_rq
from service.GameService import convert_score_sheet, repair_game_service, create_template, get_templates, get_template


@app.route('/store_template', methods=['POST'])
def template():
    json_data = request.get_json()  # src, frame
    template_rq = Template_rq(json_data)
    create_template(template_rq.img, template_rq.frame)
    return jsonify("")


@app.route('/templates', methods=['GET'])
def templates():
    # templates  = get_templates()

    return jsonify(images=get_templates())


@app.route('/convertScoreSheet', methods=['POST'])
def convert_sheet():
    json_data = request.get_json()
    score_sheet_rq = Score_sheet_rq(json_data)
    game_id, game = convert_score_sheet(score_sheet_rq.template_id, score_sheet_rq.img)
    return jsonify(id=game_id, moves=game)


@app.route('/eval', methods=['POST'])
def evaluate():
    # template_id = 1
    # template = get_template(template_id)
    # # execute only if run as a script
    #
    # evaluate_stat(template)
    i = 0
    # # # for i in range(1, 81):
    # char_graphs(i)
    # move_graphs(i)
    # game_graphs(i)
    # exact_moves_graphs(i)
    leneshtein_distance()
    # mod_leneshtein_distance()
    # time_eval()
    #
    # confution_matrix()

    return jsonify("")


@app.route('/repair_game', methods=['POST'])
def repair_game():
    json_data = request.get_json()
    game_rq = Game_rq(json_data)
    moves = repair_game_service(game_rq.game_id, game_rq.rep_move)
    return jsonify(id=game_rq.game_id, moves=moves)


# https://tinyjpg.com/

# @app.route('/')
# def app1():
#     return app.send_static_file('index.html')
#
# @app.route('/loadImage')
# def app3():
#     # return jsonify("ahoj")
#     return app.send_static_file('index.html')
#
#
#
# @app.route('/scImage')
# def app2():
#     return app.send_static_file('index.html')
