CONFIDENCE = 0.95

PATH_CONFUSION_MATRIX = ''


class Char_stat:
    def __init__(self, org_char, pred_char):

        char_stat_txt = open("stat/char_stat_conf0.txt", "a")
        self.probability = 0
        self.predicted_pos = -1
        self.probability = 0
        self.conf = 0
        if org_char == '-' or org_char == '+':
            return
        if pred_char == None or len(pred_char) == 0:
            return
        char_stat_txt.write(str(org_char) + ' ' + str(pred_char[0]) + '\n')
        char_stat_txt.close()

        for i, c in enumerate(pred_char):
            if str(org_char).lower() == str(c[0]).lower():
                self.predicted_pos = i
                self.probability = c[1]
                break
