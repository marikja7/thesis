from evaluate.Char_stat import Char_stat
from logic.Chess_logic.CMoves import levenshtein


def customive(org_move):
    len_move = len(org_move)
    if len_move > 0 and (str(org_move[len_move - 1]) == '#' or
                         str(org_move[len_move - 1]) == '+' or
                         str(org_move[len_move - 1]) == '='):
        return org_move[0:-1], 1

    return org_move, 0


class Move_stat:
    def __init__(self, game_move, org_move, pred_move, real_game_move):
        self.org_move, self.symbol = customive(org_move)
        self.before_postrprocessing_move = self.postrprocessing_move(pred_move)
        org_move = self.org_move.replace('-', '').replace('=', '')
        if org_move != '' and (org_move[-1] == '+' or org_move[-1] == '#' or org_move[-1] == '='):
            dist = levenshtein(self.before_postrprocessing_move, org_move[-1], pred_move)
        else:
            dist = levenshtein(self.before_postrprocessing_move, org_move, pred_move)
        self.distance = dist

        self.org_len = len(org_move)
        self.pred_len = len(pred_move)
        self.gm_move = 49
        self.match = 1
        if real_game_move is None or str(real_game_move.move) != str(org_move):
            self.match = 0

        self.chars = []
        for move_index, pre_move in enumerate(game_move):
            if org_move == pre_move.str_move:
                self.gm_move = move_index
                break
        # if the char is in original game move
        for char_index, pre_move in enumerate(pred_move):
            if char_index >= self.org_len:
                break
            self.chars.append(Char_stat(org_move[char_index], pred_move[char_index]))

    def postrprocessing_move(self, array):
        move = ''
        for i in array:
            move += str(i[0][0])
        return move

    def summarize(self):
        stat_array = [0] * 30
        for c in self.chars:

            if c.predicted_pos == -1:
                stat_array[29] += 1
                continue
            stat_array[c.predicted_pos] += 1

        return stat_array
