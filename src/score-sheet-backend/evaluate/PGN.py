def pgn_to_array(pgn_path):
    game = []
    with open(pgn_path + '.txt', encoding="utf8") as data_file:
        data_array = data_file.readlines()
        for d in data_array:
            if d[0] != '[':
                split_d = d.split()
                if len(split_d) == 0:
                    continue
                for m in split_d:
                    if m[-1] == '.':
                        continue
                    game.append(m)
        return game


def get_match(move, org_move):
    same = 0
    not_same = 0
    for m in move:
        if m in org_move:
            org_move.remove(m)
            same += 1
    return [same, not_same]


