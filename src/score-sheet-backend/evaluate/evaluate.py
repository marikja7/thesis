# TODO: evaluate
from evaluate.Game_ import Game_

import time

from evaluate.Game_len_stat import Game_len_stat
from evaluate.Move_ import Move_
from logic.Chess_logic.CGame import CGame
from logic.ScoreSheet import *
from logic.ScoreSheetBuilder import *
from evaluate.Game_stat import Game_stat
from logic.Img_Moves import Img_Moves
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

import numpy as np
import glob
import cv2
from logic.cnn_models.Model import Model

ROOT_PATH = '../dataset/Ricany-2020'
PATTERN_GAMES = "../dataset/Ricany-2020/*/*/*_A.JPG"
# *_[A-B]

# 1/IMG_0856_A.JPG -> IDEAL
# 1/IMG_0851_A.JPG -> malym pismem
# 1/10/IMG_0867_A. -> hodne presahuje

SPECIAL_SYMBOLS = ['1-0', '0-1', '1/2-1/2', '#', '*']


def load_files(pattern):
    return glob.glob(pattern)


# get original ogn game from dataset
def original_game_pgn(game_path):
    game = []
    pgn_path = game_path.split('.JPG')[0].split('IMG')[0]
    pgn = load_files(pgn_path + '*.pgn')[0]
    with open(pgn, encoding="utf8") as data_file:
        data_array = data_file.readlines()
        for d in data_array:
            if d[0] != '[':
                split_d = d.split()
                if len(split_d) == 0:
                    continue

                for m in split_d:
                    if m[-1] != '.' and m not in SPECIAL_SYMBOLS:
                        game.append(m)
        return game


def is_full_move(game_id, move_id, df_move):
    len_diff = int(df_move[(df_move[1] == game_id) & (df_move[2] == move_id)][0])
    return len_diff == 0


def game_graphs(index):
    df = pd.read_csv('stat/game_stat' + str(index) + '.txt', sep=" ", header=None)

    indexes = [str(i) for i in range(-5, 6)]
    indexes.insert(0, ">-5")
    indexes.append("<5")
    histogram = [0] * len(indexes)
    r_square = 0
    y_ = 0
    for (rowName, rowData) in df.iterrows():
        index = int(rowData[0]) + 6
        y_ += int(rowData[0])
        if index < 0:
            histogram[0] += 1
            continue
        if index >= len(indexes):
            histogram[len(indexes) - 1] += 1
            continue
        histogram[index] += 1

    print(histogram)
    plt.bar(indexes, histogram)

    pal = sns.color_palette("Blues", len(indexes))
    rank = np.array(histogram).argsort().argsort()  # http://stackoverflow.com/a/6266510/1628638
    sns.barplot(x=indexes, y=histogram, palette=np.array(pal)[rank])

    plt.title('Difference between parsed and real game length')
    plt.xlabel('Real game length - parsed game length')
    plt.ylabel('Count')
    # plt.show()
    plt.savefig('./graph/game_stat' + str(index) + '.jpg')
    plt.clf()


def char_graphs(index):
    df = pd.read_csv('stat/char_stat' + str(index) + '.txt', header=None)
    # df_move = pd.read_csv('stat/move_stat' + str(index) + '.txt', sep=" ", header=None)
    # print(df_move.head())
    histogram = [0] * 11
    indexes = [i for i in range(10)]
    indexes.append('<10')
    print(df.head())
    sum = 0

    for (rowName, rowData) in df.iterrows():
        for i, c in enumerate(rowData):
            c = str(c).replace('[', '').split(']')[0]
            # tahy ktere jsou delsi
            if i == 27 or i == 28:
                continue
            if i >= len(indexes) - 1:
                histogram[len(indexes) - 1] += int(c)
                continue
            histogram[i] += int(c)

    print('skipped: ', sum)
    print(histogram)
    pal = sns.color_palette("Blues", len(indexes))
    rank = np.array(histogram).argsort().argsort()  # http://stackoverflow.com/a/6266510/1628638
    sns.barplot(x=indexes, y=histogram, palette=np.array(pal)[rank])
    # plt.title('Evaluation of char prediction (only moves with good predicted len)')
    plt.title('Evaluation of char prediction')
    plt.xlabel('Position of predicted char')
    plt.ylabel('Count')
    plt.savefig('./graph/char_stat' + str(index) + '.jpg')
    plt.clf()
    # plt.show()


def move_graphs(index):
    df = pd.read_csv('stat/move_stat' + str(index) + '.txt', sep=" ", header=None)

    indexes = ['>=-4', -3, -2, -1, 0, 1, 2, 3, '<=4']
    histogram = [0] * len(indexes)
    print(df.head())
    for (rowName, rowData) in df.iterrows():
        index = int(rowData[0]) + 4
        if index < 0:
            histogram[0] += 1
            continue
        if index > len(histogram) - 1:
            histogram[len(histogram) - 1] += 1
            continue
        histogram[index] += 1
    print(histogram)
    pal = sns.color_palette("Blues", len(indexes))
    rank = np.array(histogram).argsort().argsort()  # http://stackoverflow.com/a/6266510/1628638
    sns.barplot(x=indexes, y=histogram, palette=np.array(pal)[rank])
    plt.title('Difference between parsed and real move length')
    plt.xlabel('Real move length - parsed move length')
    plt.ylabel('Count')
    # plt.show()
    plt.savefig('./graph/move_stat' + str(index) + '.jpg')
    plt.clf()


def exact_moves_graphs(index):
    df = pd.read_csv('stat/full_move_stat' + str(index) + '.txt', header=None)
    histogram = [0] * 11
    indexes = [i for i in range(10)]
    indexes.append('<10')

    print(df.head())
    for (rowName, rowData) in df.iterrows():
        for i, c in enumerate(rowData):
            if int(c) >= 10:
                histogram[10] += 1
                break
            histogram[int(c)] += 1
            break

    print(len(histogram), ' ', histogram)
    print(len(indexes), '', indexes)

    pal = sns.color_palette("Blues", 11)
    rank = np.array(histogram[:20]).argsort().argsort()  # http://stackoverflow.com/a/6266510/1628638
    sns.barplot(x=indexes[:20], y=histogram[:20], palette=np.array(pal)[rank])
    # plt.savefig('./graph/full_move_stat' + str(index) + '.png')
    # plt.title('Evaluation of char prediction (only moves with good predicted len)')
    plt.title('Evaluation of move prediction')
    plt.xlabel('Position of predicted move')
    plt.ylabel('Count')

    # plt.show()
    plt.savefig('./graph/full_move_stat' + str(index) + '.jpg')
    plt.clf()


def evaluate_stat(template):
    import time
    games_path = load_files(PATTERN_GAMES)

    t_preprocessing, t_char_segm, t_char_prediction, t_game_creation = [0]*4
    for game_id, game_path in enumerate(games_path):

        root_path = game_path.split('.JPG')[0].split('IMG')[0]
        board_game = load_files(root_path + '*.JPG')

        # Cannot process game with more score sheets
        if len(board_game) > 2:
            continue
        img = cv2.imread(game_path)

        full_time_start = time.time()
        start = time.time()
        img = build_score_sheet(img, template)
        img = clean_image(img)
        end = time.time()
        t_preprocessing += end - start
        print('path: ', game_path)
        print('t_preprocessing: ', end - start)
        org_game = original_game_pgn(game_path)


        # for i in range(0, 5):
        #     threshold = 0.5 + i/10
        start = time.time()
        moves = Img_Moves(template, img)
        end = time.time()
        t_char_segm += end - start
        print('t_char_segm: ', end - start)

        start = time.time()
        model = Model()
        predicted_moves = model.predict_moves(moves)
        end = time.time()
        t_char_prediction += end - start
        print('t_char_prediction: ', end - start)
        # game_stat = Game_len_stat(org_game, moves, game_id)
        start = time.time()
        game = CGame(predicted_moves)
        end = time.time()
        t_game_creation += end - start
        print('t_game_creation: ', end - start)
        full_time_end = time.time()

        time_txt = open("stat/time.txt", "a")
        time_txt.write(str(full_time_end-full_time_start) + ' ' + str(len(predicted_moves)) + '\n')
        time_txt.close()

        match_moves = game.cmoves.moves
        moves = []
        size = len(match_moves)
        for index_move, m in enumerate(match_moves):
            for i in m:
                act_line = 0
                if len(game.cmoves.moves) > index_move and i.str_move == game.cmoves.moves[index_move]:
                    act_line = 1
                moves.append(Move_(index_move, i.str_move, i.prob, act_line))
        db_game = Game_(size, moves)

        game_stat = Game_stat(db_game, match_moves, org_game, predicted_moves, 0)
        game_stat.save_stat(0, game_path)
    print('t_preprocessing: ', t_preprocessing)
    print('t_char_segm: ', t_char_segm)
    print('t_char_prediction: ', t_char_prediction)
    print('t_game_creation: ', t_game_creation)
    print('num_games: ', len(games_path))


def confution_matrix():
    from sklearn.metrics import confusion_matrix
    import pandas as pd
    import seaborn as sn
    import matplotlib.pyplot as plt
    import numpy as np

    df = pd.read_csv('stat/char_stat_conf0.txt', header=None, delimiter=' ')
    # histogram = [0] * 11
    # indexes = [i for i in range(10)]
    # indexes.append('<10')
    y_true = []
    y_pred = []
    print(df.head())
    for (rowName, rowData) in df.iterrows():
        # for i, c in enumerate(rowData):
        true = str(rowData[0])
        pred = str(rowData[1]).replace('(', '').split(',')[0].replace('\'', '')
        if true in ['-', '=', '0', '', ' ', '\n', '{', '}'] or pred in ['-', '=', '0', '', ' ', '\n', '{', '}']:
            continue
        y_true.append(true)

        y_pred.append(
            pred
        )

    print(np.unique(y_true))
    print(np.unique(y_pred))
    data = confusion_matrix(y_true, y_pred)
    df_cm = pd.DataFrame(data, columns=np.unique(y_true), index=np.unique(y_true))

    df_cm.index.name = 'Actual'
    df_cm.columns.name = 'Predicted'
    plt.figure(figsize=(15, 15))
    sn.set(font_scale=1.4)  # for label size
    sn.heatmap(df_cm, cmap="Blues", annot=True, annot_kws={"size": 10})  # font size
    plt.savefig('./heatmap.jpg')
    plt.clf()

def leneshtein_distance():
    df = pd.read_csv('stat/act/move_before_postprocessing0.txt', header=None, delimiter=' ')
    # df2 = pd.read_csv('stat/mode_move_before_postprocessing.txt', header=None, delimiter=' ')
    histogram = [0] * 6
    indexes = [i for i in range(5)]
    indexes.append('<5')

    print(df.head())
    for (rowName, rowData) in df.iterrows():
        for i, c in enumerate(rowData):
            if int(c) >= 5:
                histogram[5] += 1
                break
            histogram[int(c)] += 1
            break

    print(len(histogram), ' ', histogram)
    print(len(indexes), '', indexes)

    pal = sns.color_palette("Blues", 6)
    rank = np.array(histogram).argsort().argsort()  # http://stackoverflow.com/a/6266510/1628638
    sns.barplot(x=indexes, y=histogram, palette=np.array(pal)[rank])
    # plt.savefig('./graph/full_move_stat' + str(index) + '.png')
    # plt.title('Evaluation of char prediction (only moves with good predicted len)')
    plt.title('Evaluation of move prediction')
    plt.xlabel('Levenshtein distance')
    plt.ylabel('Count')

    # plt.show()
    plt.savefig('./graph/postprocessing' + str(0) + '.jpg')
    plt.clf()


def mod_leneshtein_distance():
    df = pd.read_csv('stat/mode_move_before_postprocessing0.txt', header=None, delimiter=' ')
    # df2 = pd.read_csv('stat/mode_move_before_postprocessing.txt', header=None, delimiter=' ')
    values = []

    print(df.head())
    for (rowName, rowData) in df.iterrows():
        for i, c in enumerate(rowData):
            values.append(int(c))
            break
    # pal = sns.color_palette("Blues", 10)
    sns.histplot(np.array(values), bins=10)

    # plt.show()
    plt.savefig('./graph/mod_postprocessing' + str(0) + '.jpg')
    plt.clf()

def time_eval():
    df = pd.read_csv('stat/time.txt', header=None, delimiter=' ')
    # df2 = pd.read_csv('stat/mode_move_before_postprocessing.txt', header=None, delimiter=' ')
    moves = []
    times = []

    print(df.head())
    for (rowName, rowData) in df.iterrows():
        # for i, c in enumerate(rowData):
        #     print(c)
        moves.append(int(rowData[1]))
        times.append(rowData[0])
        # break
    # sns.histplot(np.array(values), bins=10)

    # plt.show()
    sns.scatterplot(moves, times)
    plt.xlabel('Number of moves')
    plt.ylabel('Time (s)')
    plt.savefig('./graph/time.jpg')
    plt.clf()