
from evaluate.Move_stat import Move_stat
from logic.Chess_logic.CGame import CGame

import Levenshtein

class Game_stat:
    def __init__(self, game, match_moves, org_game, predicted_game, index):
        self.game = game
        self.org_game = org_game
        self.predicted_game = predicted_game

        self.org_len = len(org_game)
        self.pred_len = len(predicted_game)
        self.index = index
        self.moves = []
        for move_index, pre_game in enumerate(predicted_game):
            if move_index >= self.org_len:
                break
            if len(game.moves) <= move_index:
                self.moves.append(Move_stat(match_moves[move_index], org_game[move_index], predicted_game[move_index], None))
            else:
                self.moves.append(Move_stat(match_moves[move_index], org_game[move_index], predicted_game[move_index], game.moves[move_index]))
        self.changes = 0
        # self.repair_game()

    def equal(self, move1, move2):
        if '+' in move1 or '#' in move1:
            move1 = move1[:-1]
        if '+' in move2 or '#' in move2:
            move2 = move2[:-1]
        return move1 == move2

    def repair_game(self):
        game = CGame()

        print('Repair game------------------------------------------')
        for index in range(len(self.game.moves)):
            if index >= len(self.org_game):
                break
            if self.equal(self.game.moves[index].move, self.org_game[index]):
                continue
            # print(index, ' change moves: ', self.game.moves[index].move, ' -> ', self.org_game[index])
            # self.predicted_game = repair_moves(self.game.moves, self.org_game[index], index)

            self.changes += 1
        print('----------------------------------------------------------')

    def summarize(self):
        chars_predicted = [0] * 11

        for m in self.moves:
            array = m.summarize()
            for i in range(11):
                chars_predicted[i] += array[i]

        return chars_predicted

    def save_stat(self, game_id, path):

        move_before_postprocessing_txt = open("stat/move_before_postprocessing" + str(self.index) + ".txt", "a")
        mode_move_before_postprocessing_txt = open("stat/mode_move_before_postprocessing" + str(self.index) + ".txt", "a")
        real_game_stat_txt = open("stat/real_game_move_stat" + str(self.index) + ".txt", "a")

        char_stat_txt = open("stat/char_stat" + str(self.index) + ".txt", "a")
        move_stat_txt = open("stat/move_stat" + str(self.index) + ".txt", "a")
        game_stat_txt = open("stat/game_stat" + str(self.index) + ".txt", "a")
        # changes_txt = open("stat/changes" + str(self.index) + ".txt", "a")
        full_move_stat_txt = open("stat/full_move_stat" + str(self.index) + ".txt", "a")

        game_stat_txt.write(str(self.org_len - self.pred_len) + ' ' + str(game_id) + ' ' + str(path) + '\n')
        # changes_txt.write(str(self.changes) + '\n')
        for move_id, m in enumerate(self.moves):
            move_stat_txt.write(str(m.org_len - m.pred_len) + ' ' + str(game_id) + ' ' + str(move_id) + '\n')
            array = m.summarize()
            char_stat_txt.write(str(array) + ' ' + str(game_id) + ' ' + str(move_id) + '\n')
            full_move_stat_txt.write(str(m.gm_move) + '\n')
            real_game_stat_txt.write(str(m.match) + '\n')
            move_before_postprocessing_txt.write(str(Levenshtein.distance(m.org_move, m.before_postrprocessing_move, )) + '\n')
            mode_move_before_postprocessing_txt.write(str(m.distance) + '\n')
        real_game_stat_txt.close()
        char_stat_txt.close()
        move_stat_txt.close()
        game_stat_txt.close()
        full_move_stat_txt.close()
