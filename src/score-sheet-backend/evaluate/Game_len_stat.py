from evaluate.Move_stat import Move_stat


class Game_len_stat:
    def __init__(self, org_game, predicted_moves, index):
        self.org_len = len(org_game)
        self.pred_len = len(predicted_moves.moves)
        self.good_len = 1
        if "*" in org_game:
            self.good_len = 0
        self.index = index
        # self.moves = []
        # for move_index, pre_game in enumerate(predicted_game):
        #     if move_index >= self.org_len:
        #         break
        #     if move_index >= len(game):
        #         break
        #     self.moves.append(Move_stat(game[move_index], org_game[move_index], predicted_game[move_index]))

    def summarize(self):
        chars_predicted = [0] * 11

        for m in self.moves:
            array = m.summarize()
            for i in range(11):
                chars_predicted[i] += array[i]
        return chars_predicted

    def save_stat(self, game_id, path):

        # char_stat_txt = open("char_stat"+str(self.index)+".txt", "a")
        # move_stat_txt = open("move_stat"+str(self.index)+".txt", "a")
        game_stat_txt = open("stat/game_stat"+str(game_id)+".txt", "a")
        # full_move_stat_txt = open("full_move_stat"+str(self.index)+".txt", "a")
        if self.good_len:
            game_stat_txt.write(str(self.org_len - self.pred_len) + ' ' + str(game_id) + ' ' + str(path) + '\n')

        # for move_id, m in enumerate(self.moves):
        #     move_stat_txt.write(str(m.org_len - m.pred_len) + ' ' + str(game_id) + ' ' + str(move_id) + '\n')
        #     array = m.summarize()
        #     char_stat_txt.write(str(array) + ' ' + str(game_id) + ' ' + str(move_id) + '\n')
        #     full_move_stat_txt.write(str(m.gm_move) + '\n')
        # char_stat_txt.close()
        # move_stat_txt.close()
        game_stat_txt.close()
        # full_move_stat_txt.close()
