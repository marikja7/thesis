from database.mapper import template_to_model, game_to_model
from database.queries import SELECT_TEMPLATE_IMG
from logic.Chess_logic.CGame import CGame
from logic.ScoreSheetBuilder import build_score_sheet
from logic.ScoreSheet import clean_image
from database.model.Game import Game, db
from logic.Img_Moves import Img_Moves
from logic.cnn_models.Model import Model
from sqlalchemy import text

def convert_score_sheet(template_id, img):
    template = get_template(template_id)
    img = base64_to_matrix(img.split(',')[1])
    wraped_img = build_score_sheet(img, template)
    wraped_img = clean_image(wraped_img)
    moves = Img_Moves(template, wraped_img)
    model = Model()
    prediction = model.predict_moves(moves)
    game = CGame(prediction)
    game_model = game_to_model(game)
    db_game = save_game(game_model)
    return db_game.id, game.game


def repair_game_service(game_id, move):
    game_db = get_game(game_id)
    game_moves = game_db.moves
    game = CGame()
    game.restore_game(game_moves)
    game.repair_game(move.cnt, move.str_move)
    game_db_new = game_to_model(game)
    update_moves(game_id, game_db_new.moves)
    return game.game


# ----------------------------------------------------------------------------------------

def save_game(game):
    db.session.add(game)
    db.session.commit()
    return game


def get_game(game_id):
    return db.session.query(Game).get(game_id)

def update_moves(game_id, moves):
    game = db.session.query(Game).get(game_id)
    game.moves = moves
    db.session.commit()

from database.model.Template import Template, db
from logic.TemplateBuilder import build_template
from service.json_convert import *
from utils.Convert_img import matrix_to_base64, base64_to_matrix


def create_template(img, frame):
    img = base64_to_matrix(img.split(',')[1])
    boxes, columns, rows, move_width, move_height = build_template(img, frame.y, frame.x, frame.height, frame.width)
    img = matrix_to_base64(img)
    template_model = template_to_model(img, boxes, columns, rows, move_width, move_height)
    # Store template in database
    save_template(template_model)


# ----------------------------------------------------------------------------------------

def save_template(template):
    db.session.add(template)
    db.session.commit()


def get_template(template_id):
    return db.session.query(Template).get(template_id)


def get_templates():
    sql = text(SELECT_TEMPLATE_IMG)
    result = db.engine.execute(sql)
    db.session.close()
    return templates_to_json(result)