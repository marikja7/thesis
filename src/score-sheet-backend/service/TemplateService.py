from database.model.Template import Template, db
from database.model.Move_box import Move_box
from logic.TemplateBuilder import build_template
from service.json_convert import *
from utils.Convert_img import matrix_to_base64, base64_to_matrix
from sqlalchemy import text

def create_template(img, frame):
    img = base64_to_matrix(img.split(',')[1])
    boxes, columns, rows, move_width, move_height = build_template(img, frame.y, frame.x, frame.height, frame.width)
    move_boxes = [Move_box(cnt=box[0], x=box[1], y=box[2]) for box in boxes]
    img = matrix_to_base64(img)
    x_right = int(max(columns)+move_width)
    y_bottom = int(max(rows)+move_height)
    template = Template(img=img, x_left=int(min(columns)),
                        x_right=x_right, y_up=int(min(rows)),
                        y_bottom=y_bottom,
                        rows=int(len(rows)), columns=int(len(columns)),
                        move_width=move_width, move_height=move_height,
                        move_boxes=move_boxes)
    # Store template in database
    save_template(template)


#----------------------------------------------------------------------------------------

def save_template(template):
    db.session.add(template)
    db.session.commit()

def get_template(template_id):
    return db.session.query(Template).get(template_id)

def get_templates():
def get_templates():
    sql = text(SELECT_TEMPLATE_IMG)
    result = db.engine.execute(sql)
    db.session.close()
    return templates_to_json(result)
