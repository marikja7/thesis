from utils.Convert_img import matrix_to_response, base64_to_matrix
import cv2

HEIGHT_IMG = 500


def templates_to_json(templates):
    temp = []
    for template in templates:
        img = base64_to_matrix(template.img)
        diff = HEIGHT_IMG / img.shape[0]
        width = int(img.shape[1] * diff)
        dim = (width, HEIGHT_IMG)
        img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
        temp.append({'id': template.id, 'img': matrix_to_response(img)})
    return temp
