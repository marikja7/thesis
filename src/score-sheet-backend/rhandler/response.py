from flask import jsonify

import cv2

from utils.Convert_img import base64_to_matrix, matrix_to_response


class Templates_rs:
    def __init__(self, templates):
        HEIGHT_IMG = 500
        temp = []
        for template in templates:
            img = base64_to_matrix(template.img)
            diff = HEIGHT_IMG / img.shape[0]
            width = int(img.shape[1] * diff)
            dim = (width, HEIGHT_IMG)
            img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
            temp.append({'id': template.id, 'img': matrix_to_response(img)})
        self.response = jsonify(images=temp)

        #
# class Frame_rq:
#     def __init__(self, json_data):
#         self.y = json_data['y']
#         self.x = json_data['x']
#         self.height = json_data['height']
#         self.width = json_data['width']
#
#
# class Score_sheet_rq:
#     def __init__(self, json_data):
#         self.img = json_data['img']
#         self.template_id = json_data['template_id']
#
#
# class Game_rq:
#     def __init__(self, json_data):
#         self.game_id = json_data['game_id']
#         self.move = json_data['move']
