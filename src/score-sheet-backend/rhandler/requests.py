class Template_rq:
    def __init__(self, json_data):
        self.img = json_data['img']
        self.frame = Frame_rq(json_data['frame'])


class Frame_rq:
    def __init__(self, json_data):
        self.y = json_data['y']
        self.x = json_data['x']
        self.height = json_data['height']
        self.width = json_data['width']


class Score_sheet_rq:
    def __init__(self, json_data):
        self.img = json_data['img']
        self.template_id = json_data['template_id']


class Game_rq:
    def __init__(self, json_data):
        self.game_id = json_data['game_id']
        self.rep_move = Move_rq(json_data['move'])


class Move_rq:
    def __init__(self, json_data):
        self.cnt = json_data['cnt']
        self.str_move = json_data['move']
