from math import sqrt, pow


class Coordinates:
    def __init__(self, y, x):
        self.y = y
        self.x = x

    def __sub__(self, other):
        return Coordinates(self.y - other.y, self.x - other.x)

    def distance(self, other):
        return sqrt(pow(self.y - other.y, 2) + pow(self.x - other.x, 2))
