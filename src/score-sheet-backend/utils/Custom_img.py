# Crop empty space in image
from utils.Coordinates import Coordinates


def crop_image(image):
    height, width = image.shape
    max_x, min_x, max_y, min_y = [-1] * 4
    for w in range(width):
        for h in range(height):
            if image[h][w] == 255:
                if min_x == -1 or w < min_x:
                    min_x = w
                if min_y == -1 or h < min_y:
                    min_y = h
                if w > max_x:
                    max_x = w
                if h > max_y:
                    max_y = h

    return image[min_y:max_y + 1, min_x:max_x + 1], Coordinates(min_y, min_x)
