import cv2
import base64
from imageio import imread
import io


def matrix_to_base64(image):
    _, buffer = cv2.imencode('.jpg', image)
    return base64.b64encode(buffer)


def base64_to_matrix(image):
    return imread(io.BytesIO(base64.b64decode(image)))


def matrix_to_response(image):
    return base64_to_response(matrix_to_base64(image))


def base64_to_response(image):
    return str(image)[2:-1]
