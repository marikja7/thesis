from app import db


class Move_box(db.Model):
    __tablename__ = 'Move_box'

    id = db.Column(db.Integer, primary_key=True)
    id_template = db.Column(db.Integer, db.ForeignKey('Template.id'))
    cnt = db.Column(db.Integer)
    x = db.Column(db.Integer)
    y = db.Column(db.Integer)
