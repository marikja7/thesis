from app import db


class Game(db.Model):
    __tablename__ = 'Game'

    id = db.Column(db.Integer, primary_key=True)
    len = db.Column(db.Integer)
    moves = db.relationship('Move', lazy=False)
