from app import db


class Move(db.Model):
    __tablename__ = 'Move'

    id = db.Column(db.Integer, primary_key=True)
    id_game = db.Column(db.Integer, db.ForeignKey('Game.id'))
    cnt = db.Column(db.Integer)
    move = db.Column(db.String)
    prob = db.Column(db.Float)
    act_line = db.Column(db.Integer)
