from app import db
import json


class Template(db.Model):
    __tablename__ = 'Template'

    id = db.Column(db.Integer, primary_key=True)
    img = db.Column(db.LargeBinary)
    x_left = db.Column(db.Integer)
    x_right = db.Column(db.Integer)
    y_up = db.Column(db.Integer)
    y_bottom = db.Column(db.Integer)
    rows = db.Column(db.Integer)
    columns = db.Column(db.Integer)
    move_width = db.Column(db.Integer)
    move_height = db.Column(db.Integer)
    move_boxes = db.relationship('Move_box', lazy=False)
