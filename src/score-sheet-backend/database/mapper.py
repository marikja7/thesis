from database.model.Game import Game
from database.model.Move import Move
from database.model.Move_box import Move_box
from database.model.Template import Template


def template_to_model(img, boxes, columns, rows, move_width, move_height):
    # boxes, columns, rows, move_width, move_height = build_template(img, frame.y, frame.x, frame.height, frame.width)
    move_boxes = [Move_box(cnt=box[0], x=box[1], y=box[2]) for box in boxes]
    x_right = int(max(columns) + move_width)
    y_bottom = int(max(rows) + move_height)
    template = Template(img=img, x_left=int(min(columns)),
                        x_right=x_right, y_up=int(min(rows)),
                        y_bottom=y_bottom,
                        rows=int(len(rows)), columns=int(len(columns)),
                        move_width=move_width, move_height=move_height,
                        move_boxes=move_boxes)
    return template


def game_to_model(game) -> Game:
    match_moves = game.cmoves.moves
    moves = []
    size = len(match_moves)
    found = 0
    for index_move, m in enumerate(match_moves): 
        for i in m:
            act_line = 0

            if len(game.game) > index_move and i.str_move == game.game[index_move]:
                found = 1
                act_line = 1
            moves.append(Move(cnt=index_move, move=i.str_move, prob=round(i.prob, 3), act_line=act_line))
        if found == 0 and len(game.game) > index_move:
            moves.append(Move(cnt=index_move, move=game.game[index_move], prob=0, act_line=1))
        found = 0
    return Game(len=size, moves=moves)
