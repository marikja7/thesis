 ## Dataset
The dataset contains photos of score sheets along with corresponding PGN files. Detailed information is in **./dataset/readme.md**.

 ## Src
Folder containing source codes. In folder **./src/score-sheet-frontend/** is source code for the frontend part of application and in folder **./src/score-sheet-backend/** there is the source code for the backend part of the application. The scripts that were used for the training models, statistics, etc. are in folder **./src/notebooks/** For guidelines on how to run the app or scripts, see **./src/readme.md**.

## Text
Containes pdf version of thesis.